<?php

namespace app\modules\requerimientos\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex($idProyecto)
    {
    	$this->redirect (['/requerimientos/requerimientos','idProyecto'=>$idProyecto]);
    }
}
