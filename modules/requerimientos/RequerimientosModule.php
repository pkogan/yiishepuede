<?php

namespace app\modules\requerimientos;

class RequerimientosModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\requerimientos\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
