<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Requerimientos $model
 */

$this->title = 'Create Requerimientos';
$this->params['breadcrumbs'][] = ['label' => 'Proyectos', 'url' => ['/proyecto/index']];
$this->params['breadcrumbs'][] = ['label' => $proyecto-> Proyecto, 'url' => ['/proyecto/view','id'=>$proyecto->id]];
$this->params['breadcrumbs'][] = ['label' => 'Requerimientos', 'url' => ['index', 'idProyecto' => $idProyecto]];

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requerimientos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    	'idProyecto' => $idProyecto,
    ]) ?>

</div>
