<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Collapse;
use yii\bootstrap\Modal;
use app\widgets\NuestroNav;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Requerimientos $model
 */

$this->title = $model->nombreRequerimiento;
$this->params['breadcrumbs'][] = ['label' => 'Proyectos', 'url' => ['/proyecto/index']];
$this->params['breadcrumbs'][] = ['label' => $model->idProyectoRequerimiento0->Proyecto, 'url' => ['/proyecto/view','id'=>$model->idProyectoRequerimiento0->id]];
$this->params['breadcrumbs'][] = ['label' => 'Requerimientos', 'url' => ['/requerimientos','idProyecto'=>$model->idProyectoRequerimiento0->id]];
$this->params['breadcrumbs'][] = $this->title;

echo NuestroNav::widget(['idProyecto'=>$model->idProyectoRequerimiento0->id]);

?>
<div class="requerimientos-view">
    <h1>
    	<?= Html::encode($this->title)?>
    </h1>
    
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idRequerimientos], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idRequerimientos], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
     <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombreRequerimiento',
            'descripcion',
			
        ],
    ]) ?>
    
<div class="Collapse">
<?php
echo "</br>";
echo Collapse::widget([
	// items es un array que permite armar grupos. Cada array representa un elemento o subgrupo
    'items' => [
			//Actores
			'Actores' => [
			'content' => $model->actores,
			'contentOptions'=>['class'=>'in'],
			'options'=>['style'=>'font-color:red; border-width: medium; border-color:#000099;font-weight: bolder']
		],
			//Datos Utilizados
			'Datos Utilizados' => [
			'content'=> $model -> datosUtilizados,
			'contentOptions'=>['class'=>'in'],
			'options'=>['style'=>'border-width: medium;border-color:#000099;font-weight: bolder']
		],
			//Estado
			'Estado' => [
			'content'=> $model ->idEstadoRequerimiento,
			'contentOptions'=>['class'=>'in'],
			'options'=>['style'=>'border-width: medium;border-color:#000099;font-weight: bolder']
		],
			//Usuario
			'Usuario' => [
			'content'=> $model ->idUsuarioRequerimiento,
			'contentOptions'=>['class'=>'in'],
			'options'=>['style'=>'border-width: medium;border-color:#000099;font-weight: bolder']
		],
			//Proyecto
			'Proyecto' => [
			'content'=> $model ->idProyectoRequerimiento,
			'contentOptions'=>['class'=>'in'],
			'options'=>['style'=>'border-width: medium;border-color:#000099']
		],			
    ]
]);
?>


  </div>
