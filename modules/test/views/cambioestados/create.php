<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Cambioestados $model
 */

$this->title = 'Create Cambioestados';
$this->params['breadcrumbs'][] = ['label' => 'Cambioestados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cambioestados-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
