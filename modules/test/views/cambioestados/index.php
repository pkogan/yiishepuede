<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\Search\CambioestadosSearch $searchModel
 */

$this->title = 'Cambioestados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cambioestados-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cambioestados', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_estado',
            'id_test',
            'id_usuario',
            'descripcion',
            // 'fechahora',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
