<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Cambioestados $model
 */

$this->title = 'Update Cambioestados: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cambioestados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cambioestados-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
