<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\Search\CambioestadosSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="cambioestados-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_estado') ?>

    <?= $form->field($model, 'id_test') ?>

    <?= $form->field($model, 'id_usuario') ?>

    <?= $form->field($model, 'descripcion') ?>

    <?php // echo $form->field($model, 'fechahora') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
