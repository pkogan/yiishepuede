<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Estados;
use yii\helpers\ArrayHelper;


/**
 * @var yii\web\View $this
 * @var app\models\Cambioestados $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="cambioestados-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_estado')->dropDownList(ArrayHelper::map(Estados::find()->all(),'id','estado')) ?>    
    <?= $form->field($model, 'descripcion')->textArea(['maxlength' => 250]) ?>  

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
