<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\bootstrap\Modal;

/**
 * @var yii\web\View $this
 * @var app\models\Test $model
 */

$this->title = $model->id;
//$this->params['breadcrumbs'][] = ['label' => 'Tests', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Test', 'url' => ['/test/testing/index','idProyecto'=>$model->idProyecto]];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="test-view">
    <h1>Test <?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'id_proyecto',
            'descripcion',
            'resultadoesperado',
            'pasos',
            'prioridad',
            'idTipo.tipo',
            'fechahora',
            'idUsuarioRegistro.NombreApellido',
            //idUsuario hace referencia a la tabla USUARIO 
            //(en el modelo estan esas tablas relacionadas por medio de idUsuario), 
            //despues se pone . y el nombre del campo al que quiero acceder.
        ],
    ]) ?>

    
    <h2>Cambios de estado</h2>
 
        <?= GridView::widget([
        'dataProvider' => $dataCambioestadosProvider,
        'filterModel' => $searchCambioestadosModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'descripcion',
            'idUsuario.NombreApellido',
            'idEstado.estado',

            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{delete}',
                'controller'=>'cambio-estados',
                ]
        ],
    ]); ?>
    
    <?php Modal::begin([
    'header' => '<h2>Asignar estado al proyecto</h2>',
    'toggleButton' => ['label' => 'Nuevo Estado',
                       'class' => 'btn btn-success',
        ],
    ]);
        echo $this->render('_formcambioestados', [
        'model' => $modelCambioestados,
    ]);

    Modal::end();
 ?>

</div>
