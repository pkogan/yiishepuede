<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Tipotest;
use yii\helpers\ArrayHelper;
use app\models\Usuario;
use app\models\UsuarioProyectoFuncion;


/**
 * @var yii\web\View $this
 * @var app\models\Test $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="test-form">

    <?php $form = ActiveForm::begin(); ?>



    <?= $form->field($model, 'prioridad')->textInput(['maxlength' => 25]) ?>

    <?= $form->field($model, 'id_tipo')->dropDownList(ArrayHelper::map(Tipotest::find()->all(), 'id', 'tipo')) ?>
    
    <?= $form->field($model, 'id_usuario_registro')->dropDownList(ArrayHelper::map(Usuario::find()->all(), 'id', 'NombreApellido')) ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => 250]) ?>

    <?= $form->field($model, 'resultadoesperado')->textInput(['maxlength' => 250]) ?>

    <?= $form->field($model, 'pasos')->textInput(['maxlength' => 250]) ?>
    
    
    
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
