<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\widgets\NuestroNav;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\TestSearch $searchModel
 */


$this->title = 'Test';
$this->params['breadcrumbs'][] = ['label' => 'Proyectos', 'url' => ['/proyecto/index']];
$this->params['breadcrumbs'][] = ['label' => $proyecto->Proyecto, 'url' => ['/proyecto/view','id'=>$proyecto->id]];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="test-index">
       <?= NuestroNav::widget(['idProyecto'=>$idProyecto]); ?>
    
    <h1><?= Html::encode($this->title) ?></h1>

   

    <p>
        <?= Html::a('Create Test', ['create','idProyecto'=>$proyecto->id], ['class' => 'btn btn-success']) ?>
                    <!-- nombre     accion del controlador  parametros    
    </p>
<!--lista los test del proyecto seleccionado -->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_proyecto',
            'descripcion',
            'resultadoesperado',
            'pasos',
            'prioridad',
            'id_tipo',
            'fechahora',
            'id_usuario_registro',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
