<?php

namespace app\modules\test\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cambioestados;

/**
 * CambioestadosSearch represents the model behind the search form about `app\models\Cambioestados`.
 */
class CambioestadosSearch extends Cambioestados
{
    public function rules()
    {
        return [
            [['id', 'id_estado', 'id_test', 'id_usuario'], 'integer'],
            [['descripcion', 'fechahora'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Cambioestados::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_estado' => $this->id_estado,
            'id_test' => $this->id_test,
            'id_usuario' => $this->id_usuario,
            'fechahora' => $this->fechahora,
        ]);

        $query->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}
