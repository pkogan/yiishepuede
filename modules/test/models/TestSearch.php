<?php

namespace app\modules\test\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Test;

/**
 * TestSearch represents the model behind the search form about `app\models\Test`.
 */
class TestSearch extends Test
{
    public function rules()
    {
        return [
            [['id', 'id_proyecto', 'id_tipo', 'id_usuario_registro'], 'integer'],
            [['descripcion', 'resultadoesperado', 'pasos', 'prioridad', 'fechahora'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Test::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_proyecto' => $this->id_proyecto,
            'id_tipo' => $this->id_tipo,
            'fechahora' => $this->fechahora,
            'id_usuario_registro' => $this->id_usuario_registro,
        ]);

        $query->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'resultadoesperado', $this->resultadoesperado])
            ->andFilterWhere(['like', 'pasos', $this->pasos])
            ->andFilterWhere(['like', 'prioridad', $this->prioridad]);

        return $dataProvider;
    }
}
