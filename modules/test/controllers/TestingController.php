<?php

//namespace app\controllers;

namespace app\modules\test\controllers;

use Yii;
use app\models\Proyecto;
use app\models\Test;
use app\models\Cambioestados;
use app\models\search\TestSearch;
use app\modules\test\models\CambioestadosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TestingController implements the CRUD actions for Test model.
 */
class TestingController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Test models.
     * @return mixed
     */
    public function actionIndex($idProyecto) {
        $searchModel = new \app\modules\test\models\TestSearch;
        $dataProvider = $searchModel->search(['TestSearch' => ['id_proyecto' => $idProyecto]]);
        $model = Proyecto::findOne($idProyecto);
        return $this->render('index', [
                    'model' => $model,
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'idProyecto' => $idProyecto,
                    'proyecto' => $model,
        ]);
    }

    /**
     * Displays a single Test model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $searchCambioestadosModel = new CambioestadosSearch;
        $dataCambioestadosProvider = $searchCambioestadosModel->search(['CambioestadosSearch' => ['id_test' => $id]]);
        $modelCambioestados = $this->crearCambioestados($id);   //null
        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'modelCambioestados' => $modelCambioestados,
                    'dataCambioestadosProvider' => $dataCambioestadosProvider,
                    'searchCambioestadosModel' => $searchCambioestadosModel,
        ]);
    }

    protected function crearCambioestados($id_test) {
        $model = new Cambioestados;
        $model->id_test = $id_test;
        $model->id_usuario = Yii::$app->user->id;  //id del usuario logueado
        //exit( date('Y-m-d h:i:s'));
        //$model->fechahora = date('Y-m-d h:i:s');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //flash    
        }
        return $model;
    }

    /**
     * Creates a new Test model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    
    public function actionCreate($idProyecto) { //recibe como parametro el id del proyecto en el que estamos
        $model = new Test;
        $model->id_proyecto = $idProyecto;  
        //se le asigna el id del proyecto  que se va a testear        
        if ($model->load(Yii::$app->request->post()) && $model->save()) { 
        //carga todo lo que viene por post y lo mete en el modelo
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [ // renderiza la vista "crate" 
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Test model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Test model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Test model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Test the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Test::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
