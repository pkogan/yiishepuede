<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;

?>


<div class="wiki-default-index">
     <?php
    $items=[
    ['label' => 'edm',
     'url' => ['/edm','idProyecto'=>$model->id],
    ],
    ['label' => 'wiki',
     'url' => ['/wiki','idProyecto'=>$model->id],
    ],
    ['label' => 'rrhh',
     'url' => ['/rrhh','idProyecto'=>$model->id],
    ],
    ['label' => 'requerimientos',
     'url' => ['/requerimientos','idProyecto'=>$model->id],
    ],
    ['label' => 'metricas',
     'url' => ['/metricas','idProyecto'=>$model->id],
    ],
    ['label' => 'issue',
     'url' => ['/issue','idProyecto'=>$model->id],
    ],
    ['label' => 'testing',
     'url' => ['/testing','idProyecto'=>$model->id],
    ],
    ['label' => 'tareas',
     'url' => ['/tareas','idProyecto'=>$model->id],
    ],
];
echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $items,
            ]);?>
    <h1>Wiki</h1>
    
    
        
</div>


