<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;
use app\widgets\NuestroNav;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\modules\wiki\models\ArticuloSearch $searchModel
 */

$this->title = 'Articulos';
$this->params['breadcrumbs'][] = ['label' => 'Proyectos', 'url' => ['/proyecto/index']];
$this->params['breadcrumbs'][] = ['label' => $proyecto->Proyecto, 'url' => ['/proyecto/view','id'=>$proyecto->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articulo-index">
	<?= NuestroNav::widget(['idProyecto'=>$proyecto->id]); ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <?php echo $this->render('_search', ['model' => $searchModel],$proyecto->id); ?>

    <p>
        <?= Html::a('Crear Nuevo Articulo', ['create','id'=>$proyecto->id], ['class' => 'btn btn-success']) ?>
    
    </p>          
    
    <?= ListView::widget([
    	'dataProvider' => $dataProvider,
        'itemView'=>'view',
    ]); ?>

</div>
