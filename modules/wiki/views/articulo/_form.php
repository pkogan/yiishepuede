<!DOCTYPE html>
<html>
<head><!-- CDN hosted by Cachefly -->
<script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
<script>
        tinymce.init({selector:'textarea'});
</script>
</head>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\Articulo $model
 * @var yii\widgets\ActiveForm $form
 */

?>

<div class="articulo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php // $form->field($model, 'idProyecto')->textInput(['value'=> $model->idProyecto]) ?>

    <?= $form->field($model, 'titulo')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'texto')->textarea(['rows' => 6]) ?>

    <?php //  $form->field($model, 'fechaAlta')->textInput(['value'=>date("Y-m-d H:i:s")]) ?>

    <?php //  $form->field($model, 'fecha')->textInput(['value'=>date("Y-m-d H:i:s")]) ?>

    <?php // $form->field($model, 'idUsuario')->textInput(['value'=> Yii::$app->user->identity->id]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</html>