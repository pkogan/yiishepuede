<?php
use yii\helpers\Html;

use yii\bootstrap\Modal;
/* @var $this NoticiaController */
/* @var $model Noticia */
?>
<div class="col-md-12 ">
		<h3><?php echo Html::encode($model->titulo); ?></h3>
        <?php echo $model->texto.'<br>'; ?>
        
        <?php   
               
        Modal::begin([
            'header' => '<h3>'.$model->titulo.'</h3>'.$model->idUsuario0->NombreApellido,
            'toggleButton' => ['label' => 'Ver Articulo'],
            'footer' =>'Fecha De Alta(ultima actualizacion):  '.$model->fechaAlta.'<br>Fecha:  '.$model->fecha.'<br><hr>'.Html::a('<span class="glyphicon glyphicon-header"></span>', ['articulo-cambio/index', 'idArticulo' => $model->idArticulo, 'idProyecto'=>$model->idProyecto], ['class' => 'btn btn-success', 'data'=>['method' => 'post']]).Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->idArticulo, 'idProyecto'=>$model->idProyecto], ['class' => 'btn btn-danger', 'data'=>['method' => 'post']]).Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->idArticulo], ['class' => 'btn btn-primary']).'<button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span></button>',
            'options' => ['style' => 'background-color:rgba(0,0,0,0.2);'],        
        ]);        
        echo $model->texto;
        Modal::end();
        echo '<hr>';
        ?>
        
</div>