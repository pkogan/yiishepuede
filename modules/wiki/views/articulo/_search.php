<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\modules\wiki\models\ArticuloSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="articulo-search">

<div style="float: right; margin-right: -550px;" class="form-inline">	
    <?php $form = ActiveForm::begin([
        'method' => 'get',
    ]); ?>
    <?= $form->field($model, 'texto')->label('Buscador') ?><span class="glyphicon glyphicon-search"></span></div>
    <?php // $form->field($model, 'idArticulo') ?>

    <?php // $form->field($model, 'idProyecto') ?>

    <?php // $form->field($model, 'fechaAlta') ?>

    <?php // echo $form->field($model, 'fecha') ?>

    <?php // echo $form->field($model, 'idUsuario') ?>

    <?php ActiveForm::end(); ?>

</div>
