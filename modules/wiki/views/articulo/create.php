<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Articulo $model
 */

$this->title = 'Crear Nuevo Articulo';
$this->params['breadcrumbs'][] = ['label' => 'Proyectos', 'url' => ['/proyecto/index']];
$this->params['breadcrumbs'][] = ['label' => $proyecto->Proyecto, 'url' => ['/proyecto/view','id'=>$proyecto->id]];
$this->params['breadcrumbs'][] = ['label' => 'Articulos', 'url' => ['index','idProyecto'=>$proyecto->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articulo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
