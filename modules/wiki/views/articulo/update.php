<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Articulo $model
 */

$this->title = 'Update Articulo: ' . $model->idArticulo;
$this->params['breadcrumbs'][] = ['label' => 'Proyectos', 'url' => ['/proyecto/index']];
$this->params['breadcrumbs'][] = ['label' => $proyecto->Proyecto, 'url' => ['/proyecto/view','id'=>$proyecto->id]];
$this->params['breadcrumbs'][] = ['label' => 'Articulos', 'url' => ['index','idProyecto'=>$proyecto->id]];
$this->params['breadcrumbs'][] = ['label' => $model->titulo, 'url' => ['view', 'id' => $model->idArticulo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="articulo-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
