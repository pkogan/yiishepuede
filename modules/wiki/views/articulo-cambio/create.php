<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\ArticuloCambio $model
 */

$this->title = 'Create Articulo Cambio';
$this->params['breadcrumbs'][] = ['label' => 'Articulo Cambios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articulo-cambio-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
