<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\ArticuloCambio $model
 */

$this->title = 'Update Articulo Cambio: ' . $model->idLog;
$this->params['breadcrumbs'][] = ['label' => 'Articulo Cambios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idLog, 'url' => ['view', 'id' => $model->idLog]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="articulo-cambio-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
