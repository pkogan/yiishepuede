<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\ArticuloCambio $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="articulo-cambio-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idArticulo')->textInput() ?>

    <?= $form->field($model, 'titulo')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'texto')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'idUsuario')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
