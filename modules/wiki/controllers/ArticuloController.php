<?php

namespace app\modules\wiki\controllers;
use Yii;
use app\models\Articulo;
use app\models\Proyecto;
use app\models\Usuario;
use app\modules\wiki\models\ArticuloSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

/**
 * ArticuloController implements the CRUD actions for Articulo model.
 */
class ArticuloController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Articulo models.
     * @return mixed
     */
    public function actionIndex($idProyecto)
    {
    	$model = Proyecto::findOne($idProyecto);
    	if (!Yii::$app->user->can('usuarioVinculadoProyecto', ['Proyecto' => $model])) {
    		throw new ForbiddenHttpException('El usuario no esta vinculado al Proyecto');
    	}
    	
        $searchModel = new ArticuloSearch;
        $searchModel->idProyecto=$idProyecto;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $proyecto=  Proyecto::findOne($idProyecto);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,'proyecto'=>$proyecto,
        ]);
    }

    /**
     * Displays a single Articulo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    
    protected function crearUsuario($idArticulo) {
    	$model = new Articulo;
    	$model->idArticulo = $idArticulo;
    	$post = Yii::$app->request->post();
    	print_r($post);
    	//exit;
    	//$var = $model->find()->where('idEtapa=:idEtapa and idUsuario=:idUsuario',[':idEtapa'=>$idEtapa, ':idUsuario'=>$post['RecursoProyecto']['idUsuario']])->all();
    	if ($model->load(Yii::$app->request->post()) && ($model->findOne(['idArticulo'=>$idArticulo, 'idUsuario'=>$post['Articulo']['idUsuario']]) ||  $model->save() )) {
    		//flash
    	}
    	return $model;
    }  
    
    
    
    
    /**
     * Creates a new Articulo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new Articulo();
        $model ->idProyecto=$id;
        $model ->fecha=date('Y-m-d H:i:s');
        $model ->fechaAlta=date('Y-m-d H:i:s');
        $model ->idUsuario=Yii::$app->user->getId();
        
		$proyecto= Proyecto::findOne($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idArticulo]);
        } else {
            return $this->render('create', [
                'model' => $model,
           		'proyecto' =>$proyecto,
            ]);
        }
    }

    /**
     * Updates an existing Articulo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model ->fechaAlta=date('Y-m-d H:i:s');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idArticulo]);
        } else {
            return $this->render('update', [
                'model' => $model,
            	'proyecto' => $model->idProyecto0,
            ]);
        }
    }

    /**
     * Deletes an existing Articulo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    

    public function actionDelete($id,$idProyecto)
    {
    	$this->findModel($id)->delete();
    
    	return $this->redirect(['/wiki/articulo', 'idProyecto' => $idProyecto]);
    }
    /**
    public function actionDelete($id,$idProyecto)
    {   $unLogs = new Log();
        if (($unLogs= Log::find($id)) == null) {
        	$this->findModel($id)->delete();
        	} else {
        		return $this->redirect(['/proyecto/view', 'id' => $idProyecto]);        		
        	}
        	throw new NotFoundHttpException('No es posible eliminar este Articulo. Primero elimine los Logs asociados a el.');
            }*/

    /**
     * Finds the Articulo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Articulo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Articulo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('El articulo no existe');
        }
    }
}
