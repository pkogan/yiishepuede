<?php

namespace app\modules\wiki\controllers;
use Yii;
use app\models\Articulo;
use app\models\ArticuloCambio;
use app\models\Proyecto;
use app\models\Log;
use app\modules\wiki\models\ArticuloCambioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

/**
 * ArticuloCambioController implements the CRUD actions for ArticuloCambio model.
 */
class ArticuloCambioController extends Controller
{
    public function behaviors()
    {
		return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update', 'delete', 'create','index','view'],
                'rules' => [

                    [
                        'actions' => ['index','view','update','delete', 'create'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ArticuloCambio models.
     * @return mixed
     */
    public function actionIndex($idArticulo,$idProyecto)
    {
    	$model = Proyecto::findOne($idProyecto);
    	if (!Yii::$app->user->can('usuarioVinculadoProyecto', ['Proyecto' => $model])) {
    		throw new ForbiddenHttpException('El usuario no esta vinculado al Proyecto');
    	}
        $searchModel = new ArticuloCambioSearch;
        
        $searchModel->idArticulo=$idArticulo;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $articulos= Articulo::findOne($idArticulo);     
        $articulo=  Log::findOne($idArticulo);
        $proyecto=  Proyecto::findOne($idProyecto);

        
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        	'idArticulo'=>$articulo,
       		'proyecto'=>$proyecto,
        	'articulos'=>$articulos,
        ]);
    }

    /**
     * Displays a single ArticuloCambio model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ArticuloCambio model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ArticuloCambio;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idLog]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ArticuloCambio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idLog]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ArticuloCambio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ArticuloCambio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ArticuloCambio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ArticuloCambio::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
