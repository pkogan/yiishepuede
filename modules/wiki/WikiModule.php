<?php

namespace app\modules\wiki;

class WikiModule extends \yii\base\Module
{
    // public $defaultRoute ='wiki/articulo';
	
	public $controllerNamespace = 'app\modules\wiki\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
?>