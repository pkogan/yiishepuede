<?php

namespace app\modules\wiki\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Articulo;

/**
 * ArticuloSearch represents the model behind the search form about `app\models\Articulo`.
 */
class ArticuloSearch extends Articulo
{
    public function rules()
    {
        return [
            [['idArticulo', 'idProyecto', 'idUsuario'], 'integer'],
            [['titulo', 'texto', 'fechaAlta', 'fecha'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Articulo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (isset($this->idProyecto)){
        	$query->andFilterWhere([
        			'idProyecto' => $this->idProyecto,
					]);
        }
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'titulo',$this->texto])
            ->orFilterWhere(['like', 'texto', $this->texto]);

        return $dataProvider;
    }
}
