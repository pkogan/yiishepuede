<?php

namespace app\modules\wiki\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ArticuloCambio;

/**
 * ArticuloCambioSearch represents the model behind the search form about `app\models\ArticuloCambio`.
 */
class ArticuloCambioSearch extends ArticuloCambio
{
    public function rules()
    {
        return [
            [['idLog', 'idArticulo', 'idUsuario'], 'integer'],
            [['titulo', 'texto', 'fecha'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ArticuloCambio::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        if (isset($this->idArticulo)){
        	$query->andFilterWhere([
        			'idArticulo' => $this->idArticulo,
        			]);
        }
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idLog' => $this->idLog,
            'idArticulo' => $this->idArticulo,
            'fecha' => $this->fecha,
            'idUsuario' => $this->idUsuario,
        ]);

        $query->andFilterWhere(['like', 'titulo', $this->titulo])
            ->andFilterWhere(['like', 'texto', $this->texto]);

        return $dataProvider;
    }
}
