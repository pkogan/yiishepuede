<?php

namespace app\modules\tareas\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex($idProyecto)
    {
        $this->redirect (['/tareas/etapa-proyecto','idProyecto'=>$idProyecto]);
    }
}
