<?php

namespace app\modules\tareas\controllers;

use Yii;
use app\models\Proyecto;
use app\models\Etapaproyecto;
use app\models\RecursoProyecto;
use app\modules\tareas\models\EtapaProyectoSearch;
use app\modules\tareas\models\RecursoproyectoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\helpers\Html;
/**
 * EtapaProyectoController implements the CRUD actions for EtapaProyecto model.
 */
class EtapaProyectoController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update', 'delete', 'create','index','view'],
                'rules' => [

                    [
                        'actions' => ['index','view','update','delete', 'create'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all EtapaProyecto models.
     * @return mixed
     */
    public function actionIndex($idProyecto)
    {
        $model = Proyecto::findOne($idProyecto);
        if (!Yii::$app->user->can('usuarioVinculadoProyecto', ['Proyecto' => $model])) {
            throw new ForbiddenHttpException('El usuario no esta vinculado al Proyecto');
        }
        $searchModel = new EtapaProyectoSearch;
        $dataProvider = $searchModel->search(['EtapaProyectoSearch'=>['idProyecto'=>$idProyecto]]);//(Yii::$app->request->getQueryParams());
        //$model -> Descripcion = Html::encode($model -> Descripcion);
        $porc = 0;
        $acum = 0;
        foreach ($model->etapaproyectos as $etapa){
            $acum = $acum + 1;
            $porc = $porc + $etapa->porcentaje;
             
        }
        $porcentaje = $porc / $acum;
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'idProyecto' => $idProyecto,
            'proyecto' => $model,
            'porcentaje' => $porcentaje,
        ]);
    }

    /**
     * Displays a single EtapaProyecto model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id){
        $model = $this->findModel($id);
        if (!Yii::$app->user->can('usuarioVinculadoProyecto', ['Proyecto' => $model->idProyecto0])) {
            throw new ForbiddenHttpException('El usuario no esta vinculado al Proyecto');
        }
        $searchRecursoProyecto = new RecursoproyectoSearch;
        $dataProyectoProvider = $searchRecursoProyecto-> search(['RecursoproyectoSearch'=>['idEtapa'=>$id]]);
        $modelUsuario = $this->crearUsuario($id,$model->idProyecto0);
        return $this->render('view', [
            'model' => $model,
            'modelUsuario' => $modelUsuario,
            'dataProyectoProvider' => $dataProyectoProvider,
            'searchRecursoProyecto' => $searchRecursoProyecto,
        ]);
    }
    
    protected function crearUsuario($idEtapa,$proyecto) {
        $model = new RecursoProyecto;
        $model->idEtapa = $idEtapa;
        $post = Yii::$app->request->post();
        //$var = $model->find()->where('idEtapa=:idEtapa and idUsuario=:idUsuario',[':idEtapa'=>$idEtapa, ':idUsuario'=>$post['RecursoProyecto']['idUsuario']])->all();
        if ($model->load(Yii::$app->request->post()) && ($model->findOne(['idEtapa'=>$idEtapa, 'idUsuario'=>$post['RecursoProyecto']['idUsuario']]))) {
            if (!Yii::$app->user->can('usuarioVinculadoProyecto', ['Proyecto' => $proyecto, 'Funcion' => Proyecto::LIDERPROYECTO])) {
                throw new ForbiddenHttpException('El usuario no tiene permisos para asignar usuarios al Proyecto');
            }
            if ($model->save()) {
            //flash
            }
        }
        return $model;
    }
    /**
     * Creates a new EtapaProyecto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($idProyecto)
    {
        $proyecto = new Proyecto;
        $proyecto = Proyecto::findOne($idProyecto);
        $model = new Etapaproyecto;
        $model ->idProyecto=$idProyecto;
          //print_r(Yii::$app->request->post());exit;
        if ($model->load(Yii::$app->request->post())) {
            //print_r(Yii::$app->request->post());
            
            if (!Yii::$app->user->can('usuarioVinculadoProyecto', ['Proyecto' => $proyecto, 'Funcion' => Proyecto::LIDERPROYECTO])) {
                throw new ForbiddenHttpException('El usuario no tiene permisos para asignar usuarios al Proyecto');
            }
            if ($model->save()) {
            //flash
            }
            return $this->redirect(['index', 'idProyecto' => $idProyecto]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing EtapaProyecto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idEtapa]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing EtapaProyecto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if (!Yii::$app->user->can('usuarioVinculadoProyecto', ['Proyecto' => $model->idProyecto0, 'Funcion' => Proyecto::LIDERPROYECTO])) {
                throw new ForbiddenHttpException('El usuario no tiene permisos para asignar usuarios al Proyecto');
            }
        $idProyecto = $model->idProyecto;
        $this->findModel($id)->delete();

        return $this->redirect(['index', 'idProyecto' => $idProyecto]);
    }

    /**
     * Finds the EtapaProyecto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EtapaProyecto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Etapaproyecto::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
