<?php

namespace app\modules\tareas\controllers;

use Yii;
use app\models\Recursoproyecto;
use app\modules\tareas\models\RecursoproyectoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RecursoproyectoController implements the CRUD actions for Recursoproyecto model.
 */
class RecursoproyectoController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Recursoproyecto models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RecursoproyectoSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Recursoproyecto model.
     * @param integer $idEtapa
     * @param integer $idUsuario
     * @return mixed
     */
    public function actionView($idEtapa, $idUsuario)
    {
        return $this->render('view', [
            'model' => $this->findModel($idEtapa, $idUsuario),
        ]);
    }

    /**
     * Creates a new Recursoproyecto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Recursoproyecto;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idEtapa' => $model->idEtapa, 'idUsuario' => $model->idUsuario]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Recursoproyecto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $idEtapa
     * @param integer $idUsuario
     * @return mixed
     */
    public function actionUpdate($idEtapa, $idUsuario)
    {
        $model = $this->findModel($idEtapa, $idUsuario);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idEtapa' => $model->idEtapa, 'idUsuario' => $model->idUsuario]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Recursoproyecto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $idEtapa
     * @param integer $idUsuario
     * @return mixed
     */
    public function actionDelete($idEtapa, $idUsuario)
    {
        $this->findModel($idEtapa, $idUsuario)->delete();

        return $this->redirect(['/tareas/etapa-proyecto/view','id'=>$idEtapa]);
    }

    /**
     * Finds the Recursoproyecto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $idEtapa
     * @param integer $idUsuario
     * @return Recursoproyecto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idEtapa, $idUsuario)
    {
        if (($model = Recursoproyecto::findOne(['idEtapa' => $idEtapa, 'idUsuario' => $idUsuario])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
