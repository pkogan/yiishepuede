<?php

namespace app\modules\tareas;

class TareasModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\tareas\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
