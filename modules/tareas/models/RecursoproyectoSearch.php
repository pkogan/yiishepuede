<?php

namespace app\modules\tareas\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Recursoproyecto;

/**
 * RecursoproyectoSearch represents the model behind the search form about `app\models\Recursoproyecto`.
 */
class RecursoproyectoSearch extends Recursoproyecto
{
    public function rules()
    {
        return [
            [['idEtapa', 'idUsuario'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params=null)
    {
        $query = Recursoproyecto::find();
        $query->joinWith(['idUsuario0']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idEtapa' => $this->idEtapa,
            'idUsuario' => $this->idUsuario,
        ]);

        return $dataProvider;
    }
}
