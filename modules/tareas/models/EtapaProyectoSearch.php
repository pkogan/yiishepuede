<?php

namespace app\modules\tareas\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Etapaproyecto;

/**
 * EtapaProyectoSearch represents the model behind the search form about `app\models\EtapaProyecto`.
 */
class EtapaProyectoSearch extends Etapaproyecto
{
    public function rules()
    {
        return [
            [['idEtapa', 'idProyecto'], 'integer'],
            [['nombre', 'descripcion', 'fechaInicio', 'fechaFin'], 'safe'],
            [['porcentaje'], 'number'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = EtapaProyecto::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idEtapa' => $this->idEtapa,
            'idProyecto' => $this->idProyecto,
            'fechaInicio' => $this->fechaInicio,
            'fechaFin' => $this->fechaFin,
            'porcentaje' => $this->porcentaje,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }

    
}