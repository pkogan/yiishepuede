<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\bootstrap\Modal;

/**
 * @var yii\web\View $this
 * @var app\models\EtapaProyecto $model
 */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Etapa Proyectos', 'url' => ['/tareas/etapa-proyecto/index','idProyecto'=>$model->idProyecto]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="etapa-proyecto-view">

    <h1>Etapa <?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idEtapa], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idEtapa], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'idEtapa',
            //'idProyecto',
            'nombre',
            'descripcion:ntext',
            'fechaInicio',
            'fechaFin',
            
        ],
    ]) ?>

    <h3>Porcentaje de la Etapa:</h3>
    <div class="progress">
    <div class="progress-bar"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: <?= $model->porcentaje?>%">
    <span style="min-width">
	<?= $model->porcentaje?>
	% Complete</span>
    </div>
    </div>
    
    
    <h3>Usuarios asignados a la Etapa</h3>
    <?php Modal::begin([
    'header' => '<h2>Asignar Usuario al proyecto</h2>',
    'toggleButton' => ['label' => 'Nuevo Usuario',
                       'class' => 'btn btn-success',
        ],
    ]);
        echo $this->render('_formusuarioetapa', [
        'model' => $modelUsuario,
        'proyecto' => $model->idProyecto0,
    ]);

    Modal::end();
 ?>
    
    
    <?= GridView::widget([
        'dataProvider' => $dataProyectoProvider,
        'filterModel' => $searchRecursoProyecto,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idEtapa0.nombre',
            'idUsuario0.NombreApellido',

            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{delete}',
                'controller'=>'recursoproyecto'
                ],
        ],
    ]); ?>
</div>
