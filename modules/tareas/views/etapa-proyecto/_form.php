<!DOCTYPE html>
<html>
<head><!-- CDN hosted by Cachefly -->
<script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
<script>
        tinymce.init({selector:'textarea'});
</script>
</head>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/**
 * @var yii\web\View $this
 * @var app\models\EtapaProyecto $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="etapa-proyecto-form">

    <?php $form = ActiveForm::begin(); ?>

    

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => 1000]) ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>
    
    <?= $form->field($model, 'fechaInicio')->textInput() ?>
    
    <?= $form->field($model, 'porcentaje')->textInput() ?>

    <?= $form->field($model, 'fechaFin')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</html>