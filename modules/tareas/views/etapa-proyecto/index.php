<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\widgets\NuestroNav;
use yii\bootstrap\Progress;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\modules\tareas\models\EtapaProyectoSearch $searchModel
 */

$this->title = 'Etapa Proyectos';
$this->params['breadcrumbs'][] = ['label' => 'Proyectos', 'url' => ['/proyecto/index']];
$this->params['breadcrumbs'][] = ['label' => $proyecto->Proyecto, 'url' => ['/proyecto/view','id'=>$proyecto->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="etapa-proyecto-index">
    <?= NuestroNav::widget(['idProyecto'=>$idProyecto]); ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <!--<?php echo $this->render('_search', ['model' => $searchModel]); ?>-->

    <p>
        <?= Html::a('Crear Etapa', ['create', 'idProyecto' => $idProyecto], ['class' => 'btn btn-success']) ?>
    </p>

   
    
    
    <?php 
    echo Progress::widget([
        'percent' => $porcentaje,
        'label' => 'Proyecto: '.$porcentaje.'%',
    ]);
    ?>
   
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idEtapa',
            //'idProyecto0.Proyecto',
            'nombre',
            'descripcion:ntext',
            'fechaInicio',
            'fechaFin',
            //'porcentaje',
            
            
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}{delete}{tareas}',
            ],
            
        ],
    ]); ?>
    
    
</div>
