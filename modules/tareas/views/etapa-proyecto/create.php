<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\EtapaProyecto $model
 */

$this->title = 'Create Etapa Proyecto';
$this->params['breadcrumbs'][] = ['label' => 'Etapa Proyectos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="etapa-proyecto-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'proyecto' => $model->idProyecto,
    ]) ?>

</div>
