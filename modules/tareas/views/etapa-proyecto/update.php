<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\EtapaProyecto $model
 */

$this->title = 'Update Etapa Proyecto: ' . $model->idEtapa;
$this->params['breadcrumbs'][] = ['label' => 'Etapa Proyectos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idEtapa, 'url' => ['view', 'id' => $model->idEtapa]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="etapa-proyecto-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
