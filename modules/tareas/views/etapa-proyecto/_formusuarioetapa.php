<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Usuario;
use app\models\Funciones;
use yii\helpers\ArrayHelper;
use app\models\UsuarioProyectoFuncion;
/**
 * @var yii\web\View $this
 * @var app\models\UsuarioProyectoFuncion $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="usuario-proyecto-funcion-form">

    <?php $form = ActiveForm::begin(); ?>

    
    <?php
    $usuarios=UsuarioProyectoFuncion::find()->with('idUsuario0')->where('idProyecto=:idProyecto', [':idProyecto'=>$proyecto->id])->distinct()->all();   
    $select=array();
    foreach ($usuarios as $usuario){
        /*
         * @var app\models\Usuario $usuario 
         */
        $select[$usuario->idUsuario]=$usuario->idUsuario0->Usuario;
    }
    //print_r($select);
    echo $form->field($model, 'idUsuario')->dropDownList($select);
    ?>
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>