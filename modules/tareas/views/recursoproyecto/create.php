<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Recursoproyecto $model
 */

$this->title = 'Create Recursoproyecto';
$this->params['breadcrumbs'][] = ['label' => 'Recursoproyectos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recursoproyecto-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
