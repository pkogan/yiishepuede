<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Recursoproyecto $model
 */

$this->title = 'Update Recursoproyecto: ' . $model->idEtapa;
$this->params['breadcrumbs'][] = ['label' => 'Recursoproyectos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idEtapa, 'url' => ['view', 'idEtapa' => $model->idEtapa, 'idUsuario' => $model->idUsuario]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="recursoproyecto-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
