<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\Recursoproyecto $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="recursoproyecto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idEtapa')->textInput() ?>

    <?= $form->field($model, 'idUsuario')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
