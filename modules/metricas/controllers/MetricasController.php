<?php

namespace app\modules\metricas\controllers;

use Yii;
use app\models\Proyecto;
use app\models\Metricas;
use app\models\RecursoProyecto;
use app\modules\metricas\models\MetricasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RequerimientosController implements the CRUD actions for Requerimientos model.
 */
class MetricasController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Requerimientos models.
     * @return mixed
     */
    public function actionIndex($idProyecto)
    {
    	$proyecto = Proyecto::findone($idProyecto);
    	
        $searchModel = new MetricasSearch;
        $dataProvider = $searchModel->search(['MetricasSearch'=>['idProyectoRequerimiento' => $idProyecto]]);
        $costo=0;
        $costoReal=0;
        foreach ($proyecto->requerimientos as $req){
            //$acum = $acum + 1;
            $costo = $costo + $req->CostoEstimadoHoras;
            $costoReal = $costoReal + $req->costoReal;
             
        }
        //$porcentaje = $porc / $acum;
         
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'proyecto' => $proyecto,
            'costo' => $costo,
            'costoReal' => $costoReal,
            //'totalHoras' => $searchModel->getTotals([$idProyecto]),
            //'ValorHora' => $proyecto->ValorHora,
        ]);
    }

    /**
     * Displays a single Requerimientos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model=$this->findModel($id);
        $proyecto = Proyecto::findone($model->idProyectoRequerimiento);
        return $this->render('view', [
            'model' => $model,
            'proyecto' => $proyecto,
        ]);
    }

    /**
     * Creates a new Requerimientos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($idProyecto)
    {
        $model = new Metricas;
        $proyecto = Proyecto::findone($idProyecto);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            	'proyecto' => $proyecto,
	        'idProyecto' => $idProyecto,
            ]);
        }
    }
    
    public function actionBandeja(){
    	
    	$idUsuario = Yii::$app->user->getId();
    	 
    	$searchModelEntry = new MetricasSearch;
    	$dataProviderEntry = $searchModelEntry->search(['MetricasSearch'=>['idUsuarioRequerimiento' => $idUsuario]]);
    	
    	return $this->render('bandeja', [
    			'dataProvider' => $dataProviderEntry,
    			'searchModel' => $searchModelEntry,
    			]);
    }

    /**
     * Updates an existing Requerimientos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idRequerimientos]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Requerimientos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['/metricas/view','id'=>$idRequerimiento]);
    }

    /**
     * Finds the Requerimientos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Requerimientos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Metricas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
