<?php

namespace app\modules\metricas\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex($idProyecto)
    {
    	$this->redirect (['/metricas/metricas','idProyecto'=>$idProyecto]);
    }
}
