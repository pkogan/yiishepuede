<?php

namespace app\modules\metricas\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ActiveQuery;
use app\models\Metricas;

/**
 * RequerimientosSearch represents the model behind the search form about `app\models\Requerimientos`.
 */
class MetricasSearch extends Metricas
{
	public $proyecto;
	public $estado;
	
    public function rules()
    {
        return [
            [['idRequerimientos', 'idComplejidadRequerimiento', 'idEstadoRequerimiento', 'idUsuarioRequerimiento', 'idProyectoRequerimiento'], 'integer'],
            [['nombreRequerimiento', 'actores', 'descripcion', 'datosUtilizados','proyecto','estado'], 'safe'],
            [['costoReal', 'costoRealFinal'], 'number'],
            
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Metricas::find();
        $query -> joinWith('idProyectoRequerimiento0');
        $query -> joinWith('idEstadoRequerimiento0');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idRequerimientos' => $this->idRequerimientos,
            'costoReal' => $this->costoReal,
            'idComplejidadRequerimiento' => $this->idComplejidadRequerimiento,
            'idEstadoRequerimiento' => $this->idEstadoRequerimiento,
            'idUsuarioRequerimiento' => $this->idUsuarioRequerimiento,
            'idProyectoRequerimiento' => $this->idProyectoRequerimiento,
        ]);

        $query->andFilterWhere(['like', 'nombreRequerimiento', $this->nombreRequerimiento])
               ->andFilterWhere(['like', 'actores', $this->actores])
               ->andFilterWhere(['like', 'descripcion', $this->descripcion])
               ->andFilterWhere(['like', 'datosUtilizados', $this->datosUtilizados])
               ->andFilterWhere(['like', 'idProyectoRequerimiento0.Proyecto', $this->proyecto])
               ->andFilterWhere(['like', 'idEstadoRequerimiento0.DescripcionEstado', $this->estado]);

        return $dataProvider;
    }
    
     
   
     
}
