<?php

namespace app\modules\metricas;

class MetricasModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\metricas\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
