<?php

namespace app\modules\metricas\views\metricas;

use app\modules\metricas\models;
use app\modules\metricas\controllers;
use yii\helpers\Html;
use yii\grid\GridView;
use app\widgets\NuestroNav;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\RequerimientosSearch $searchModel
 */

$this->title = 'Bandeja de Entrada';
$this->params['breadcrumbs'][] = ['label' => 'Proyectos', 'url' => ['/proyecto/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requerimientos-index">
	
	
    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php  //echo $this->render('_search', ['model' => $searchModel]); ?>
   
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			//['header'=>'Proyecto','value'=>
			'idProyectoRequerimiento0.Proyecto',
            'idRequerimientos',
            'nombreRequerimiento',
            'actores',
            'descripcion',
            //'costoReal',
            'datosUtilizados',
            //'idComplejidadRequerimiento',
            'idEstadoRequerimiento0.DescripcionEstado',
            //'idUsuarioRequerimiento0.Usuario',
            //'idProyectoRequerimiento',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
