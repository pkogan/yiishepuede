<?php

namespace app\modules\metricas\views\metricas;

use app\modules\metricas\models;
use app\modules\metricas\controllers;
use yii\helpers\Html;
use yii\grid\GridView;
use app\widgets\NuestroNav;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\RequerimientosSearch $searchModel
 */
$this->title = 'Metricas';
$this->params['breadcrumbs'][] = ['label' => 'Proyectos', 'url' => ['/proyecto/index']];
$this->params['breadcrumbs'][] = ['label' => $proyecto->Proyecto, 'url' => ['/proyecto/view', 'id' => $proyecto->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requerimientos-index">
<?= NuestroNav::widget(['idProyecto' => $proyecto->id]); ?>

    <h1><?= Html::encode($this->title) ?></h1>
    <small>En este modulo solo se permite modificar el estado y carga horaria real de los requerimientos previamente cargados.</small><br />
<?php //echo $this->render('_search', ['model' => $searchModel]);  ?>
    <p>
    <?php // Html::a('Crear Requerimiento', ['create', 'idProyecto' => $proyecto->id], ['class' => 'btn btn-success'])  ?>
    </p>
        <?php
        //print_r($proyecto->getcostoEstimadoHorass); 
        //print_r($searchModel->getcostoEstimadoHoras());
        // print_r($totalHoras);
        echo 'El valor de Hora configurado para este proyecto es $' . $proyecto->ValorHora;
        echo '<br>A este proyecto se le dedican ' . $proyecto->HorasSemanales . ' horas semanales.';
        echo '<br>Este proyecto tenia estipulado un costo de ' . $costo . ' horas de desarrollo por un valor de $' . $proyecto->ValorHora * $costo . '.';
        echo '<br>Hasta el momento se requirieron de ' . $costoReal . ' horas de desarrollo por un valor de $' . $proyecto->ValorHora * $costoReal . '.<br /><br />';
        // $crph=$dataProvider->costoRealProyHoras();
        //print_r($searchModel);
        ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'idRequerimientos',
            'nombreRequerimiento',
            //'actores',
            'descripcion',
            'idEstadoRequerimiento0.DescripcionEstado',
            //'costoReal',
            
            'CostoEstimadoHoras',
            'CostoEstimadoPesos',
            'costoReal',
            'CostoRealPesos',
            
            // 'ValorEstimado'=>$proyecto->ValorHora ,
            //'costoReal'*$proyecto->ValorHora,
            //'datosUtilizados',
            //'idComplejidadRequerimiento',
            // 'idEstadoRequerimiento0.DescripcionEstado',
            'idUsuarioRequerimiento0.Usuario',
            //'idProyectoRequerimiento',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>



</div>
