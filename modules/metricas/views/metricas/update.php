<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Requerimientos $model
 */

$this->title = 'Update: ' . $model->nombreRequerimiento;
$this->params['breadcrumbs'][] = ['label' => 'Proyectos', 'url' => ['/proyecto/index']];
$this->params['breadcrumbs'][] = ['label' => $model->idProyectoRequerimiento0->Proyecto, 'url' => ['/proyecto/view','id'=>$model->idProyectoRequerimiento0->id]];
$this->params['breadcrumbs'][] = ['label' => 'Metricas', 'url' => ['/metricas','idProyecto'=>$model->idProyectoRequerimiento0->id]];
$this->params['breadcrumbs'][] = ['label' => $model->nombreRequerimiento, 'url' => ['view', 'id' => $model->idRequerimientos]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="requerimientos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
