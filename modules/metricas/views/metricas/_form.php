<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Estadorequerimiento;
use app\models\Complejidad;
use app\models\Proyecto;
use app\models\Usuario;
use yii\helpers\ArrayHelper;


/**
 * @var yii\web\View $this
 * @var app\models\Requerimientos $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="requerimientos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombreRequerimiento')->textInput(['maxlength' => 100]) ?>

    <?php // $form->field($model, 'costoReal')->textInput() ?>
    <?= $form->field($model, 'costoReal')->textInput() ?>
    
    <?= $form->field($model, 'idComplejidadRequerimiento')->dropDownList(ArrayHelper::map(Complejidad::find()->all(),'idComplejidad','descripcionComplejidad'));?>

    <?php // $form->field($model, 'idComplejidadRequerimiento')->textInput() ?>
    
    <?= $form->field($model, 'idEstadoRequerimiento')->dropDownList(ArrayHelper::map(Estadorequerimiento::find()->all(),'idEstado','DescripcionEstado'));?>

    <?php // $form->field($model, 'idEstadoRequerimiento')->textInput() ?>
    
    <?php // $form->field($model, 'idUsuarioRequerimiento')->dropDownList(ArrayHelper::map(Usuario::find()->all(),'id','Usuario'));?>

    <?php // $form->field($model, 'idUsuarioRequerimiento')->textInput() ?>
    
    <?php // $form->field($model, 'idProyectoRequerimiento')->dropDownList(ArrayHelper::map(Proyecto::find()->all(),'id','Proyecto'));?>

    <?php // $form->field($model, 'idProyectoRequerimiento')->textInput() ?>

    <?php // $form->field($model, 'actores')->textInput(['maxlength' => 100]) ?>

    <?php // $form->field($model, 'datosUtilizados')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
