<?php

namespace app\modules\rrhh\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex($idProyecto)
    {    	
    	$this->redirect(['/rrhh/dedicacion', 'idProyecto'=>$idProyecto]);
        
    }
}
