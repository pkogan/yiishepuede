<?php

namespace app\modules\rrhh\controllers;

use Yii;
use app\models\rrhh_dedicacion;
use app\modules\rrhh\models\Rrhh_DedicacionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Proyecto;
use app\models\Etapaproyecto;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

/**
 * DedicacionController implements the CRUD actions for rrhh_dedicacion model.
 */
class DedicacionController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update', 'delete', 'create','index','view'],
                'rules' => [

                    [
                        'actions' => ['index','view','update','delete', 'create'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all rrhh_dedicacion models.
     * @return mixed
     */
    public function actionIndex($idProyecto)
    {
        $proyecto = Proyecto::findOne($idProyecto);
		   
    	$searchModel = new Rrhh_DedicacionSearch;
    	$dataProvider = $searchModel->listaSearch(Yii::$app->request->getQueryParams(), $idProyecto);
    	
    	return $this->render('index', [
    			'dataProvider' => $dataProvider,
    			'searchModel' => $searchModel,
    			'proyecto' => $proyecto,
    			
    			
        ]);
    }

    /**
     * Displays a single rrhh_dedicacion model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $unaDedicacion = $this->findModel($id);
        $unaEtapa = new Etapaproyecto;
        $unaEtapa = Etapaproyecto::findOne($unaDedicacion->idEtapa);
        $Proyecto = Proyecto::findOne($unaEtapa->idProyecto);
       
        
    	return $this->render('view', [
            'model' => $unaDedicacion,
    		'idProyecto' => $unaEtapa->idProyecto,
    		'Proyecto' => $Proyecto,	
        	
        ]);
    }

    /**
     * Creates a new rrhh_dedicacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($idProyecto)
    {
        $model = new rrhh_dedicacion;
        $Proyecto = Proyecto::findOne($idProyecto);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            	'idProyecto' => $idProyecto,
            	'Proyecto' => $Proyecto,
            ]);
        }
    }

    /**
     * Updates an existing rrhh_dedicacion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $unaEtapa = new Etapaproyecto;
        $unaEtapa = Etapaproyecto::findOne($model->idEtapa);
        $Proyecto = Proyecto::findOne($unaEtapa->idProyecto);
        

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            	'Proyecto' => $Proyecto,
            	
            	
            ]);
        }
    }
    /**
     * Deletes an existing rrhh_dedicacion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the rrhh_dedicacion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return rrhh_dedicacion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = rrhh_dedicacion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
