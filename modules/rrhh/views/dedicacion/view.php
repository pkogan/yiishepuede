<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\widgets\NuestroNav;
/**
 * @var yii\web\View $this
 * @var app\models\rrhh_dedicacion $model
 */

$this->title = $model->dedicacion;
$this->params['breadcrumbs'][] = ['label' => 'Proyectos', 'url' => ['/proyecto/index']];
$this->params['breadcrumbs'][] = ['label' => $Proyecto->Proyecto, 'url' => ['/proyecto/view','id'=>$Proyecto->id]];
$this->params['breadcrumbs'][] = ['label' => $model->idEtapa0->nombre, 'url' => ['/tareas/etapa-proyecto/view','id'=>$model->idEtapa]];
$this->params['breadcrumbs'][] = ['label' => 'Recusos Humanos', 'url' => ['index', 'idProyecto'=>$model->idEtapa0->idProyecto]];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="rrhh-dedicacion-view">
<?= NuestroNav::widget(['idProyecto'=>$model->idEtapa0->idProyecto]);?>
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'idEtapa0.nombre',
            'idUsuario0.NombreApellido',
            'dedicacion',
            'hora',
            'fecha',
            'descripcion',
        ],
    ]) ?>

</div>
