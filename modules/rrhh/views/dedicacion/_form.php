<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Proyecto;
use app\models\Etapaproyecto;
use app\models\Usuario;
use yii\helpers\ArrayHelper;
use app\models\rrhh_dedicacion;
use app\models\UsuarioProyectoFuncion;
use app\assets\AppAsset;
/**
 * @var yii\web\View $this
 * @var app\models\rrhh_dedicacion $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="rrhh-dedicacion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idEtapa')->dropDownList(ArrayHelper::map(Etapaproyecto::find()->where('idProyecto=:idProyecto', [':idProyecto'=>$idProyecto])->all(),'idEtapa','nombre')) ?>

    <?php
     // $form->field($model, 'idUsuario')->dropDownList(ArrayHelper::map(Usuario::find()->all(),'id','Usuario')) 
		
		if (Yii::$app->user->identity->username == 'admin'){
			$usuarios=UsuarioProyectoFuncion::find()->joinWith('idUsuario0')->where('idProyecto=:idProyecto', [':idProyecto'=>$idProyecto])->distinct()->all();
			$select=array();
			foreach ($usuarios as $usuario){
				/*
	 				* @var app\models\Usuario $usuario
				*/
				$select[$usuario->idUsuario]=$usuario->idUsuario0->Usuario;
			}
			//print_r($select);
			echo $form->field($model, 'idUsuario')->dropDownList($select);
		}else{
			
			 $form->field($model, 'idUsuario')->textInput(['value'=>Yii::$app->user->id;]); 
		}
	?>

    <?= $form->field($model, 'dedicacion')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'hora')->textInput() ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => 1000]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
