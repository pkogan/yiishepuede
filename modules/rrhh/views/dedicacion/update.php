<?php

use yii\helpers\Html;
use app\widgets\NuestroNav;

/**
 * @var yii\web\View $this
 * @var app\models\rrhh_dedicacion $model
 */

$this->title = 'Update: ' . $model->dedicacion;
$this->params['breadcrumbs'][] = ['label' => 'Proyectos', 'url' => ['/proyecto/index']];
$this->params['breadcrumbs'][] = ['label' => $Proyecto->Proyecto, 'url' => ['/proyecto/view','id'=>$Proyecto->id]];
$this->params['breadcrumbs'][] = ['label' => $model->idEtapa0->nombre, 'url' => ['/tareas/etapa-proyecto/view','id'=>$model->idEtapa]];
$this->params['breadcrumbs'][] = ['label' => 'Recusos Humanos', 'url' => ['index', 'idProyecto'=>$model->idEtapa0->idProyecto]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rrhh-dedicacion-update">
<?= NuestroNav::widget(['idProyecto'=>$model->idEtapa0->idProyecto]);?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    	'idProyecto' => $model->idEtapa0->idProyecto,
    ]) ?>

</div>
