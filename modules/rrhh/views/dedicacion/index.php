<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\widgets\NuestroNav;
use yii\widgets\ListView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\modules\rrhh\models\Rrhh_DedicacionSearch $searchModel
 */

$this->title = 'Recursos Humanos';
$this->params['breadcrumbs'][] = ['label' => 'Proyectos', 'url' => ['/proyecto/index']];
$this->params['breadcrumbs'][] = ['label' => $proyecto->Proyecto, 'url' => ['/proyecto/view','id'=>$proyecto->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rrhh-dedicacion-index">
	<?= NuestroNav::widget(['idProyecto'=>$proyecto->id]);?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Dedicacion', ['create', 'idProyecto' => $proyecto->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?=  GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idEtapa0.idProyecto',
            'idEtapa0.nombre',
            'idUsuario0.NombreApellido',
            'dedicacion',
            'hora',
            'fecha',
            'descripcion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); 
    
    ?>

</div>
