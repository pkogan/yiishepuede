<?php

use yii\helpers\Html;
use app\widgets\NuestroNav;

/**
 * @var yii\web\View $this
 * @var app\models\rrhh_dedicacion $model
 */

$this->title = 'Crear Dedicacion';
$this->params['breadcrumbs'][] = ['label' => 'Proyectos', 'url' => ['/proyecto/index']];
$this->params['breadcrumbs'][] = ['label' => $Proyecto->Proyecto, 'url' => ['/proyecto/view','id'=>$Proyecto->id]];
$this->params['breadcrumbs'][] = ['label' => 'Recusos Humanos', 'url' => ['index', 'idProyecto'=>$idProyecto]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rrhh-dedicacion-create">
<?= NuestroNav::widget(['idProyecto'=>$idProyecto]);?>
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    	'idProyecto' => $idProyecto,
    ]) ?>

</div>
