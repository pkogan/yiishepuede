<?php

namespace app\modules\rrhh;

class RrhhModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\rrhh\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
