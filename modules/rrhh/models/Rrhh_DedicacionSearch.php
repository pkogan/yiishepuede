<?php

namespace app\modules\rrhh\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\rrhh_dedicacion;
use yii\helpers\ArrayHelper;
use app\models\Proyecto;

/**
 * Rrhh_DedicacionSearch represents the model behind the search form about `app\models\rrhh_dedicacion`.
 */
class Rrhh_DedicacionSearch extends rrhh_dedicacion
{
	
    public function rules()
    {
        return [
            [['id', 'idEtapa', 'idUsuario'], 'integer'],
            [['dedicacion', 'fecha', 'descripcion'], 'safe'],
            [['hora'], 'number'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
     public function listaSearch($params, $idProyecto){
     // $usuarios=UsuarioProyectoFuncion::find()->with('idUsuario0')->where('idProyecto=:idProyecto', [':idProyecto'=>$proyecto->id])->distinct()->all();   
    
     	$query = rrhh_dedicacion::find();
     	$query->joinWith('idEtapa0');
     	
     	$dataProvider = new ActiveDataProvider([
     			'query' => $query,
     			]);
     	$query->andFilterWhere([
     			'etapaproyecto.idProyecto'=> $idProyecto,
     			]);
     	if (!($this->load($params) && $this->validate())) {
     		return $dataProvider;
     	}
     	
     	$query->andFilterWhere([
     			'id' => $this->id,
     			'idEtapa' => $this->idEtapa,
     			'idUsuario' => $this->idUsuario,
     			'hora' => $this->hora,
     			'fecha' => $this->fecha,
     			//'etapaproyecto.idProyecto'=> $idProyecto,
     			]);
     	
     	$query->andFilterWhere(['like', 'dedicacion', $this->dedicacion])
     	->andFilterWhere(['like', 'descripcion', $this->descripcion]);
     	//$query->andFilterWhere(['etapaproyecto.idProyecto', $this->idProyecto]);
     	return $dataProvider;
     }

    public function search($params)
    {
        $query = rrhh_dedicacion::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'idEtapa' => $this->idEtapa,
            'idUsuario' => $this->idUsuario,
            'hora' => $this->hora,
            'fecha' => $this->fecha,
        ]);

        $query->andFilterWhere(['like', 'dedicacion', $this->dedicacion])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}
