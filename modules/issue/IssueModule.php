<?php

namespace app\modules\issue;

class IssueModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\issue\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
