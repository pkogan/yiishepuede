<?php

namespace app\modules\edm;

class EdmModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\edm\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
