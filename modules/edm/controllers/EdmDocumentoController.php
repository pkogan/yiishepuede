<?php

namespace app\modules\edm\controllers;


use Yii;
use app\models\EdmDocumento;
use app\modules\edm\models\EdmDocumentoSearchSearch;
use app\models\EdmCambios;
use app\models\Usuario;
use app\models\UsuarioSearch;
use app\models\UsuarioProyectoFuncion;
use app\models\UsuarioProyectoFuncionSearch;
use app\modules\edm\models\EdmCambiosSearch;
use yii\web\Controller;
use app\models\Proyecto;
use app\models\ProyectoSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\base\theme;
use yii\base\UserException;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;
/**
 * EdmDocumentoController implements the CRUD actions for EdmDocumento model.
 */
class EdmDocumentoController extends Controller
{
    public function behaviors()
    {
        //FUNCION NUEVA
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update', 'delete', 'create','index','view'],
                'rules' => [

                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['administrador'],
                    ],
                    [
                        'actions' => ['index','view','update', 'create'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
        
        
        //-------------- FUNCION VIEJA -----------------//
        
        /*return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];*/
        
        //-----------------------------------------------//
    }

    
    /**
     * Lists all EdmDocumento models.
     * @return mixed
     */
    public function actionIndex($idProyecto)
    {
        //$usuario = Yii::$app->user->identity->username; //para traer el username del usuario logueado
        //Yii::$app->user->identity->rol //para traer el rol, que no lo sabiamos
        $searchModel = new EdmDocumentoSearchSearch; //controlador para las busuquedas del EDM Documento 
        if (!Yii::$app->user->can('administrador')) {
            $searchModel->documento_activo = 1;

        }
        $searchModel->documento_proyecto = $idProyecto; 
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $model = Proyecto::findOne($idProyecto);        

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'proyecto' => $model,
            'idProyecto' => $idProyecto,
            
        ]);
    }

     //----------------------------//
    //-------------------------------//
    public function actionView($id)
    {   
        
        $model = $this->findModel($id);
        if (!Yii::$app->user->can('usuarioVinculadoProyecto', ['Proyecto' => $model->documentoProyecto])) {
            throw new ForbiddenHttpException('El usuario no esta vinculado al Proyecto');
        }

        $searchModel2 = new EdmCambiosSearch;
        $dataProvider2 = $searchModel2->search(['EdmCambiosSearch'=>['cambio_documento'=>$id]]/*Yii::$app->request->getQueryParams()*/);
        return $this->render('view', [
            'model' => $model,
            'searchModel2' => $searchModel2,
            'dataProvider2' => $dataProvider2,
            
        ]);
    }

    /**
     * Creates a new EdmDocumento model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($idProyecto)
    {

        
        $model = new EdmDocumento;
        //Crea una nueva instancia de la clase EdmDocumento
        $band = false;
        //Crea una bandera con valor falso

        if(isset($_POST['EdmDocumento'])){ //Entra al if si hay alguna variable enviada por post con el nombre 'EdmDocumento'
    
            $model->load(Yii::$app->request->post());
            //A la instancia creada $model se le asigna todos los datos que vienen por post del formulario
            
            $file = UploadedFile::getInstance($model,'documento_archivo');
            //UploadedFile es una clase que representa la informacion de un archivo subido
            //A $file le asignamos la informacion del archivo subido
            
            if(is_object($file)){ //Comprueba si la variable $file es un objeto                   
                
                $extenssion = substr($file->name, -4); //Selecciona los ultimos 4 caracteres para obtener la extension y la asigna a la variable
                $nombreCifrado = md5(microtime()); //Cifra en md5 la variable entregada por la funcion microtime y la asigna a la variable
                $nombreCifrado .= $extenssion ; //Adiciona a la variable anterior la extension del archivo
                $file->name = $nombreCifrado;//md5($file->name).$extenssion;
                $path = Yii::$app->basePath.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'edm'.DIRECTORY_SEPARATOR.'archivos';
                //Especificamos la ruta en donde se encuentran los archivos 
                //Yii:: Es una colección de funciones auxiliares útiles para Yii Framework
                //$app->basePath Nos da el directorio raiz de la aplicacion
                
                if(!is_dir($path)) //is_dir indica si el nombre de la ruta es un directorio, si es falso entra en el if 
                mkdir($path); //Crea un directorio con la ruta dada si no existe
                                     
                /* --------------------------------- */                                      
                $url   = $path.DIRECTORY_SEPARATOR.$file->name; //url definitiva al archivo (Donde guarda el archivo)                                                            
                $array = pathinfo($url); //Devuelve la informacion del URL como un array asociativo y la asigna a la variable
                $ext   = $array['extension']; //Asigna la extension a una variable
                $ext   = strtolower($ext); //Pasa a minuscula la extension                     
                /* --------------------------------- */                                      
                 
                if(empty($ext) OR $ext != 'txt' && $ext != 'pdf' &&  $ext != 'png' && $ext != 'jpg' && $ext != 'gif'){ 
                    //Si la extension esta vacia o es diferente de alguna de las extensiones escritas entra por el if
                      throw new \yii\web\HttpException(204, " Solo puede subir archivos con extension (.txt, .pdf, .png, .jpg, .gif)");
                      //Muestra un error por subir un archivo con extension no  valida
                }
                else{                         
                     $file->saveAs($url); //Si entra por el else guarda el archivo en la ruta especificada
                     $band = true; //Cambia la bandera con el valor true
                }     
                                      
                /* --------------------------------------- */
                $model->documento_archivo    = $file->name; //Al modelo con el tag documento_archivo se le asigna el nombre del archivo

                //echo($model->documento_archivo); exit;                 
                //$model->documento_usuario    = Yii::$app->user->getState('idUsuario');
                
                $model->documento_usuario    = Yii::$app->user->getId(); //Al modelo con el tag documento_usuario se le asigna el id del usuario
                $model->documento_activo     = 1;  //Al modelo con el tag documento_activo se le asigna 1 y siempre se pone como activo
                $model->documento_fechayhora = date('Y-m-d H:i:s'); //Asignamos a documento_fechayhora el el tiempo y fecha con la funcion date
                $model->documento_proyecto = $idProyecto; //Al modelo con el tag documento_proyecto se le asigna el id del proyecto que entra por post
                
                //especifica los privilegios al crear los archivos;                
                if (Yii::$app->user->can('cliente')) { //Si el usuario es cliente entra al if
                    $model->documento_privilegios = 1; //Le asigna a documento_privilegios el valor 1(Publico)

                }
                else {
                    $model->documento_privilegios = 2; //Le asigna a documento_privilegios el valor 2(Privado)
                };
                                        
            };                  
        }
                    if ($band && $model->save()){ //Si $band es true y pudo guardar los datos del modelo entra por el if
                        return $this->redirect(['view', 'id' => $model->documento_id]);} //Redirecciona a la vista del documento creado
                    else {
                        return $this->render('create', [ //Si entra por el else vuelve a renderizar la vista create
                            'model' => $model,
                        ]);
                    }                       
    }

        

    /**
     * Updates an existing EdmDocumento model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        if (!empty($_POST['EdmDocumento'])) {

            
            $cambio = new EdmCambios;                       
              $cambio->cambio_documento   = $model->documento_id;  //referencia al documento actual
              $cambio->cambio_nombre      = $model->documento_nombre; //nombre del documento del modelo documento
              $cambio->cambio_archivo     = $model->documento_archivo; //nombre del archivo en el modelo documento
              $cambio->cambio_fechayhora  = $model->documento_fechayhora; // deberia ser automatica en la insercion a la database
              $cambio->cambio_usuario     = $model->documento_usuario; //documento del usuario que hizo el cambio
              $cambio->cambio_privilegios = $model->documento_privilegios; //deberian ser los mismos ? creo q si
              $cambio->cambio_descripcion = $model->documento_descripcion; //descripcion del cambio o la antigua.
              $cambio->cambio_activo      = $model->documento_activo; //si el documento se encuentra activo o no.
              if (Yii::$app->user->can('cliente')) {
                    $cambio->cambio_privilegios = 1;
                }
                else {
                    $cambio->cambio_privilegios = 2;
                };
                            
              if($cambio->save()) //si se guarda el actual documento en el cambio
              {
                  
                  $model->attributes = $_POST['EdmDocumento'];
                                    
                  /* ---- Para el archivo y su ruta  ---*/
                  $file = UploadedFile::getInstance($model,'documento_archivo');
                  if(is_object($file)) //significa que recupero el archivo subido                      
                  {                        
                    /* ----------------------------------- */      
                      
                    $extenssion = substr($file->name, -4);
                    $nombreCifrado = md5(microtime());
                    $nombreCifrado .= $extenssion ; 
                    $file->name = $nombreCifrado;//md5($file->name).$extenssion;
                    $path = Yii::$app->basePath.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'edm'.DIRECTORY_SEPARATOR.'archivos';                     
                    if(!is_dir($path))
                            mkdir($path);
                                                                                  
                    $url = $path.DIRECTORY_SEPARATOR.$file->name; //url definitiva al archivo
                    /* ----------------------------------- */                                                 
                    $array = pathinfo($url);                     
                    $ext   = $array['extension'];
                    $ext   = strtolower($ext);
                                          
                    if(empty($ext) OR $ext != 'txt' && $ext != 'pdf' &&  $ext != 'png' && $ext != 'jpg' && $ext != 'gif'){                         
                         throw new HttpException(204, " Solo puede subir archivos con extension (.txt, .pdf, .png, .jpg, .gif");                          
                    }
                    else{                                            
                        $file->saveAs($url);  
                        $guardoArchivo = true;
                    }                                     
                    /* ----------------------------------- */                                                                                              
                    if(!empty($guardoArchivo))
                    {                    
                        $model->documento_archivo = $file->name;                                                               
                        if($model->save())
                            $this->redirect(array('view','id'=>$model->documento_id)); 
                        else
                            throw new HttpException(102, "Pudo guardar el archivo pero no la referencia en la BBDD");
                    }
                    else
                       throw new HttpException(204, "Hubo un problema al cargar guardar el archivo");                                                               
                  }                      
               }
               else {
                   echo("eno exite accion todavia");
                   exit;
               }
            
        }
 else {
    return $this->render('update', [
                'model' => $model,
            ]);
}

    }

    //--------------------------------------------------------------------------------//
    //................................................................................//
    
    public function actionDescargar($cambio_archivo)
    {

                $path = Yii::$app->basePath.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'edm'.DIRECTORY_SEPARATOR.'archivos'.DIRECTORY_SEPARATOR;   
                //Especificamos la ruta en donde se encuentran los archivos 
                //Yii:: Es una colección de funciones auxiliares útiles para Yii Framework
                //$app->basePath Nos da el directorio raiz de la aplicacion
                $file = $path.$cambio_archivo;
                //Concatena la ruta con el archivo a descargar($cambio_archivo)
                
                header('Content-disposition: attachment; filename='.$file);
                //Content-disposition proporciona un nombre de fichero recomendado y fuerza al navegador el mostarar el diálogo para guardar el fichero
                header("Content-Type: application/force-download");                
                
                $dirInfo = pathinfo($file); 
                //pathinfo Devuelve información acerca de la ruta de un fichero como un array asociativo, o bien como un string
                $nombre = $dirInfo['basename'];                                            
                $ext = $dirInfo['extension']; 
                //Asigna la extension del archivo a una variable
                                
                if(strtolower($ext) == 'pdf') //Pasa la extension a minuscula y la compara si es igual a pdf
                    header('Content-type: application/pdf'); //Si es igual a pdf abre la aplicacion para mostrar el pdf en la pagina                                                
                            
                header("Content-disposition: attachment; filename=".$nombre);
                //Content-disposition proporciona un nombre de fichero recomendado y fuerza al navegador el mostarar el diálogo para guardar el fichero 
                
                $file = file_get_contents($file);
                //file_get_contents() es la manera preferida de transmitir el contenido de un fichero a una cadena. Usa técnicas de mapeado de memoria, para mejorar el rendimiento
                
                echo $file;
                die;         //Muere la funcion(Termina)         

            }
        
    
    /**
     * Deletes an existing EdmDocumento model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        /*----------parte que venia por defecto--------------*/
        /*$this->findModel($id)->delete();

        return $this->redirect(['index']);*/
        
        $model = $this->findModel($id);
        $model->documento_activo= 0;
        return $this->redirect(['index']);

    }

    
    public function actionAlta($documento_activo) //da de alta algun archivo que este de baja
    {        
        $model = $this->findModel($documento_activo);
        $model->documento_activo= 1;
        return $this->redirect(['index']);

    }
    
    protected function findModel($id)
    {
        if (($model = EdmDocumento::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('La pagina requerida no existe.');
        }
    }      
        
}
