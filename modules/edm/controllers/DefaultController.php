<?php

namespace app\modules\edm\controllers;

use yii\web\Controller;
use app\models\Proyecto;

class DefaultController extends Controller
{

    public function actionIndex($idProyecto)
    {
    	$this->redirect(['/edm/edm-documento', 'idProyecto'=>$idProyecto]);
    	$model = Proyecto::findOne($idProyecto);
        return $this->render('index', ['model'=> $model]);
    }
}
