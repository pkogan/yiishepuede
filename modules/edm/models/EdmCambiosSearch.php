<?php

namespace app\modules\edm\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EdmCambios;

/**
 * EdmDocumentoSearchSearch represents the model behind the search form about `app\models\EdmDocumento`.
 */
class EdmCambiosSearch extends EdmCambios
{
    public function rules()
    {
        return [
            [['cambio_id', 'cambio_documento', 'cambio_usuario', 'cambio_privilegios', 'cambio_activo'], 'integer'],
            [['cambio_nombre', 'cambio_archivo', 'cambio_fechayhora', 'cambio_descripcion'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = EdmCambios::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'cambio_id' => $this->cambio_id,
            'cambio_documento' => $this->cambio_documento,
            'cambio_nombre' => $this->cambio_nombre,
            'cambio_archivo' => $this->cambio_archivo,
            'cambio_fechayhora' => $this->cambio_fechayhora,
            'cambio_usuario' => $this->cambio_usuario,
            'cambio_privilegios' => $this->cambio_privilegios,
            'cambio_descripcion' => $this->cambio_descripcion,
            'cambio_activo' => $this->cambio_activo,
        ]);

        $query->andFilterWhere(['like', 'cambio_nombre', $this->cambio_nombre])
            ->andFilterWhere(['like', 'cambio_archivo', $this->cambio_archivo])
            ->andFilterWhere(['like', 'cambio_descripcion', $this->cambio_descripcion]);

        return $dataProvider;
    }
}
