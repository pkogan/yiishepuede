<?php

namespace app\modules\edm\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EdmDocumento;

/**
 * EdmDocumentoSearchSearch represents the model behind the search form about `app\models\EdmDocumento`.
 */
class EdmDocumentoSearchSearch extends EdmDocumento {

    public function rules() {
        return [
            [['documento_id', 'documento_usuario', 'documento_privilegios', 'documento_activo', 'documento_proyecto'], 'integer'],
            [['documento_nombre', 'documento_archivo', 'documento_fechayhora', 'documento_descripcion'], 'safe'],
        ];
    }

    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params) {
        $query = EdmDocumento::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $query->andFilterWhere([
            'documento_proyecto' => $this->documento_proyecto,
            'documento_activo' => $this->documento_activo,
        ]);
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'documento_id' => $this->documento_id,
            'documento_fechayhora' => $this->documento_fechayhora,
            'documento_usuario' => $this->documento_usuario,
            'documento_privilegios' => $this->documento_privilegios,
            'documento_activo' => $this->documento_activo,
                //'documento_proyecto' => $this->documento_proyecto,
        ]);

        $query->andFilterWhere(['like', 'documento_nombre', $this->documento_nombre])
                ->andFilterWhere(['like', 'documento_archivo', $this->documento_archivo])
                ->andFilterWhere(['like', 'documento_descripcion', $this->documento_descripcion]);

        return $dataProvider;
    }

}
