<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\EdmCambiosSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="edm-cambios-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cambio_id') ?>

    <?= $form->field($model, 'cambio_documento') ?>

    <?= $form->field($model, 'cambio_nombre') ?>

    <?= $form->field($model, 'cambio_archivo') ?>

    <?= $form->field($model, 'cambio_fechayhora') ?>

    <?php // echo $form->field($model, 'cambio_usuario') ?>

    <?php // echo $form->field($model, 'cambio_privilegios') ?>

    <?php // echo $form->field($model, 'cambio_descripcion') ?>

    <?php // echo $form->field($model, 'cambio_activo') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
