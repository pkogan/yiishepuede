<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\EdmCambios $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="edm-cambios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cambio_documento')->textInput() ?>

    <?= $form->field($model, 'cambio_nombre')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'cambio_archivo')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'cambio_fechayhora')->textInput() ?>

    <?= $form->field($model, 'cambio_usuario')->textInput() ?>

    <?= $form->field($model, 'cambio_privilegios')->textInput() ?>

    <?= $form->field($model, 'cambio_descripcion')->textInput(['maxlength' => 500]) ?>

    <?= $form->field($model, 'cambio_activo')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
