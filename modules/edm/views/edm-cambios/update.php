<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\EdmCambios $model
 */

$this->title = 'Update Edm Cambios: ' . $model->cambio_id;
$this->params['breadcrumbs'][] = ['label' => 'Edm Cambios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cambio_id, 'url' => ['view', 'id' => $model->cambio_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="edm-cambios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
