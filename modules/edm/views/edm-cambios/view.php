<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\EdmCambios $model
 */

$this->title = $model->cambio_id;
$this->params['breadcrumbs'][] = ['label' => 'Edm Cambios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="edm-cambios-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->cambio_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->cambio_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cambio_id',
            'cambio_documento',
            'cambio_nombre',
            'cambio_archivo',
            'cambio_fechayhora',
            'cambio_usuario',
            'cambio_privilegios',
            'cambio_descripcion',
            'cambio_activo',
        ],
    ]) ?>

</div>
