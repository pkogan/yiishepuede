<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\EdmCambios $model
 */

$this->title = 'Create Edm Cambios';
$this->params['breadcrumbs'][] = ['label' => 'Edm Cambios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="edm-cambios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
