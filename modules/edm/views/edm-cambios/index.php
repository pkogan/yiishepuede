<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\EdmCambiosSearch $searchModel
 */

$this->title = 'Edm Cambios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="edm-cambios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Edm Cambios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cambio_id',
            'cambio_documento',
            'cambio_nombre',
            'cambio_archivo',
            'cambio_fechayhora',
            // 'cambio_usuario',
            // 'cambio_privilegios',
            // 'cambio_descripcion',
            // 'cambio_activo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
