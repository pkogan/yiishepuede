<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\EdmDocumento $model
 */

$this->title = 'Update Edm Documento: ' . $model->documento_id;
$this->params['breadcrumbs'][] = ['label' => 'Edm Documentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->documento_id, 'url' => ['view', 'id' => $model->documento_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="edm-documento-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
