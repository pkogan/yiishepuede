<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\helpers\Url;
use app\widgets\NuestroNav;
use app\models\Proyecto;

/**
 * @var yii\web\View $this
 * @var app\models\EdmDocumento $model
 */

$this->title = $model->documento_nombre;
$this->params['breadcrumbs'][] = ['label' => 'Proyectos', 'url' => ['/proyecto/index']];
$this->params['breadcrumbs'][] = ['label' => $model->documentoProyecto->Proyecto, 'url' => ['/proyecto/view','id'=>$model->documentoProyecto->id]];
$this->params['breadcrumbs'][] = ['label' => 'Edm Documentos', 'url' => ['/edm/edm-documento/index', 'idProyecto'=>$model->documentoProyecto->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="edm-documento-view">
    

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->documento_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->documento_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Realmente estas seguro que queres borrar estos items?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Descargar', ['edm-documento/descargar','cambio_archivo' => $model->documento_archivo], ['class' => 'btn btn-primary'])
        //Crea un boton descargar que usa la funcion descargar en EdmDocumentoController y manda como variable 'cambio_archivo' que es el nombre del archivo a descargar?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'documento_id',
            'documento_nombre',
            'documento_archivo',
            'documento_fechayhora',
            'documentoUsuario.Usuario',
            'documento_privilegios',
            'documento_descripcion',
            
            
            //'documento_activo',
            //'documento_proyecto',
        ],
    ]) ?>
    
   
    <br>
    <?=GridView::widget([
        'dataProvider' => $dataProvider2,
        'filterModel' => $searchModel2,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'], //buscador

            //'cambio_id',
            //'cambio_documento',
            'cambio_nombre',
            'cambio_archivo',
            'cambio_fechayhora',
            'cambioUsuario.Usuario',
            // 'cambio_privilegios',
            // 'cambio_descripcion',
            // 'cambio_activo',

            ['class' => 'yii\grid\ActionColumn',
             'template' => '{Descargar}',
             'buttons' => [           
                
                'Descargar' => function ($url, $model) {
                    return Html::a('Descargar', ['edm-documento/descargar','cambio_archivo' => $model->cambio_archivo]);
                    //Crea un link descargar que usa la funcion descargar en EdmDocumentoController y manda como variable 'cambio_archivo' que es el nombre del archivo a descargar
                    
                }
            
                 
            ], // view, edit , delete
        ],
    ]]);
            Yii::$app->urlManager->createUrl(['site/page', 'id' => 'about'])?>;

</div>
