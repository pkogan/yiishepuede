<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\EdmDocumento $model
 */

$this->title = 'Create Edm Documento';
$this->params['breadcrumbs'][] = ['label' => 'Edm Documentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="edm-documento-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
