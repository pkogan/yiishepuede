<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\EdmDocumento $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="edm-documento-form">

    <?php $form = ActiveForm::begin([ 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'documento_nombre')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'documento_archivo')->fileInput() ?>
      
    <?php
    $items = [0 , 1];
    if (Yii::$app->user->can('administrador')) {
       echo $form->field($model, 'documento_activo')->radioList($items);
    }
    ?>
    

    <?= $form->field($model, 'documento_descripcion')->textInput(['maxlength' => 500]) ?>




    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
