<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveField;
use app\widgets\NuestroNav;
use app\models\Proyecto;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\EdmDocumentoSearchSearch $searchModel
 */

$this->title = 'Edm Documentos';
$this->params['breadcrumbs'][] = ['label' => 'Proyectos', 'url' => ['/proyecto/index']];
$this->params['breadcrumbs'][] = ['label' => $proyecto->Proyecto, 'url' => ['/proyecto/view','id'=>$proyecto->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="edm-documento-index">
    
    <?= NuestroNav::widget(['idProyecto'=>$proyecto->id]);?>
    
    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Edm Documento', ['create','idProyecto'=>$proyecto->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    
     $columns=[  
            ['class' => 'yii\grid\SerialColumn'],

            //'documento_id',
            'documento_nombre',
            'documento_archivo',
            'documento_fechayhora',
            'documentoUsuario.Usuario',
            // 'documento_privilegios',
            // 'documento_descripcion',
         ];
         if (Yii::$app->user->can('administrador')) {
             array_push($columns, 'documento_activo');
             //$columns[]='documento_activo';
         };
          $columns[]=['class' => 'yii\grid\ActionColumn'];
        
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]); ?>
    
<?php
 /*
 * @HACER: info para poner para tener como tarea para la proxima vez que te pongas a programar
 */

?> 
</div>
