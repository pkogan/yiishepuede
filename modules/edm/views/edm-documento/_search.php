<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\EdmDocumentoSearchSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="edm-documento-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'documento_id') ?>

    <?= $form->field($model, 'documento_nombre') ?>

    <?= $form->field($model, 'documento_archivo') ?>

    <?= $form->field($model, 'documento_fechayhora') ?>

    <?= $form->field($model, 'documento_usuario') ?>

    <?php // echo $form->field($model, 'documento_privilegios') ?>

    <?php // echo $form->field($model, 'documento_descripcion') ?>

    <?php // echo $form->field($model, 'documento_activo') ?>

    <?php // echo $form->field($model, 'documento_proyecto') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
