<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'extensions' => require(__DIR__ . '/../vendor/yiisoft/extensions.php'),
    'modules' => [
        'edm' => [
            'class' => 'app\modules\edm\EdmModule',
        ],
        'requerimientos' => [
            'class' => 'app\modules\requerimientos\RequerimientosModule',
        ],
        'rrhh' => [
            'class' => 'app\modules\rrhh\RrhhModule',
        ],
        'wiki' => [
            'class' => 'app\modules\wiki\WikiModule',
        ],
        'metricas' => [
            'class' => 'app\modules\metricas\MetricasModule',
        ],
         'issue' => [
            'class' => 'app\modules\issue\IssueModule',
        ],
        'tareas' => [
            'class' => 'app\modules\tareas\TareasModule',
        ],
        'test' => [
            'class' => 'app\modules\test\TestModule',
        ],
    ],
    'components' => [
        
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\Usuario',
            'enableAutoLogin' => true,
        ],
        'authManager' => [
            'class' => 'app\models\UsuarioAuth',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mail' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
