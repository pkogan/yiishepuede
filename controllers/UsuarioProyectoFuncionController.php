<?php

namespace app\controllers;

use Yii;
use app\models\UsuarioProyectoFuncion;
use app\models\Proyecto;
use app\models\UsuarioProyectoFuncionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

/**
 * UsuarioProyectoFuncionController implements the CRUD actions for UsuarioProyectoFuncion model.
 */
class UsuarioProyectoFuncionController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['delete'],
                'rules' => [
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UsuarioProyectoFuncion models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new UsuarioProyectoFuncionSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single UsuarioProyectoFuncion model.
     * @param integer $idUsuario
     * @param integer $idProyecto
     * @param integer $idFuncion
     * @return mixed
     */
    public function actionView($idUsuario, $idProyecto, $idFuncion) {
        return $this->render('view', [
                    'model' => $this->findModel($idUsuario, $idProyecto, $idFuncion),
        ]);
    }

    /**
     * Creates a new UsuarioProyectoFuncion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new UsuarioProyectoFuncion;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idUsuario' => $model->idUsuario, 'idProyecto' => $model->idProyecto, 'idFuncion' => $model->idFuncion]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UsuarioProyectoFuncion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $idUsuario
     * @param integer $idProyecto
     * @param integer $idFuncion
     * @return mixed
     */
    public function actionUpdate($idUsuario, $idProyecto, $idFuncion) {
        $model = $this->findModel($idUsuario, $idProyecto, $idFuncion);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idUsuario' => $model->idUsuario, 'idProyecto' => $model->idProyecto, 'idFuncion' => $model->idFuncion]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UsuarioProyectoFuncion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $idUsuario
     * @param integer $idProyecto
     * @param integer $idFuncion
     * @return mixed
     */
    public function actionDelete($idUsuario, $idProyecto, $idFuncion) {
        $usuarioProyectoFuncion = $this->findModel($idUsuario, $idProyecto, $idFuncion);
        if (!Yii::$app->user->can('usuarioVinculadoProyecto', ['Proyecto' => $usuarioProyectoFuncion->idProyecto0, 'Funcion' => Proyecto::LIDERPROYECTO])) {
            throw new ForbiddenHttpException('El usuario no tiene permisos para borrar usuarios del Proyecto');
        }
        $usuarioProyectoFuncion->delete();

        return $this->redirect(['proyecto/view', 'id' => $idProyecto]);
    }

    /**
     * Finds the UsuarioProyectoFuncion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $idUsuario
     * @param integer $idProyecto
     * @param integer $idFuncion
     * @return UsuarioProyectoFuncion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idUsuario, $idProyecto, $idFuncion) {
        if (($model = UsuarioProyectoFuncion::findOne(['idUsuario' => $idUsuario, 'idProyecto' => $idProyecto, 'idFuncion' => $idFuncion])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
