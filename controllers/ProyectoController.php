<?php

namespace app\controllers;

use Yii;
use app\models\Proyecto;
use app\models\ProyectoSearch;
use app\models\UsuarioProyectoFuncionSearch;
use app\models\UsuarioProyectoFuncion;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

/**
 * ProyectoController implements the CRUD actions for Proyecto model.
 */
class ProyectoController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update', 'delete', 'create','index','view'],
                'rules' => [

                    [
                        'actions' => ['delete', 'create'],
                        'allow' => true,
                        'roles' => ['administrador'],
                    ],
                    [
                        'actions' => ['index','view','update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Proyecto models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ProyectoSearch;
        //print_r(Yii::$app->request->getQueryParams());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Proyecto model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $model = $this->findModel($id);
        if (!Yii::$app->user->can('usuarioVinculadoProyecto', ['Proyecto' => $model])) {
            throw new ForbiddenHttpException('El usuario no esta vinculado al Proyecto');
        }
        $searchUsuarioModel = new UsuarioProyectoFuncionSearch;
        //$searchUsuarioModel->idProyecto=$id;
        $dataUsuarioProvider = $searchUsuarioModel->search(['UsuarioProyectoFuncionSearch'=>['idProyecto'=>$id]]);//Yii::$app->request->getQueryParams());
        
        $modelUsuario = $this->crearUsuario($model);
        return $this->render('view', [
                    'model' => $model,
                    'modelUsuario' => $modelUsuario,
                    'dataUsuarioProvider' => $dataUsuarioProvider,
                    'searchUsuarioModel' => $searchUsuarioModel,
        ]);
    }

    protected function crearUsuario($proyecto) {
        $model = new UsuarioProyectoFuncion;
        $model->idProyecto = $proyecto->id;
        if ($model->load(Yii::$app->request->post())){
            if (!Yii::$app->user->can('usuarioVinculadoProyecto', ['Proyecto' => $proyecto, 'Funcion' => Proyecto::LIDERPROYECTO])) {
                throw new ForbiddenHttpException('El usuario no tiene permisos para asignar usuarios al Proyecto');
            }
            if ($model->save()) {
            //flash
                
            }
        }
        return $model;
    }

    /**
     * Creates a new Proyecto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Proyecto;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Proyecto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if (!Yii::$app->user->can('usuarioVinculadoProyecto', ['Proyecto' => $model,'Funcion' => Proyecto::LIDERPROYECTO])) {
            throw new ForbiddenHttpException('El usuario no puede editar el Proyecto');
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Proyecto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Proyecto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Proyecto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Proyecto::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
