-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 26-05-2014 a las 19:10:29
-- Versión del servidor: 5.5.35-0ubuntu0.12.04.2
-- Versión de PHP: 5.5.11-2+deb.sury.org~precise+2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `YiiShePuede`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE IF NOT EXISTS `articulo` (
  `idArticulo` int(10) NOT NULL AUTO_INCREMENT,
  `idProyecto` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `fechaAlta` datetime NOT NULL,
  `fecha` datetime NOT NULL,
  `idUsuario` int(11) NOT NULL,
  PRIMARY KEY (`idArticulo`),
  KEY `idProyecto` (`idProyecto`),
  KEY `idUsuario` (`idUsuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`idArticulo`, `idProyecto`, `titulo`, `texto`, `fechaAlta`, `fecha`, `idUsuario`) VALUES
(8, 2, 'Prueba de articulo', 'Ese es otro muchacho que desayuna con ginebra todas las mañanas porque no tiene conciencia clara de lo que dice.- ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(10, 3, 'Nuevo articulo', 'Al respecto, remarcó que es "desastrosa y cavernícola la política económica que lleva adelante el Gobierno" y alertó que la Argentina está en un "proceso de recesión muy importante".', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(12, 4, 'prueba 4', '"Hoy tenemos los mejores precios internacionales en años y tenemos productores desfinanciados, con pérdidas, con quebrantos", lamentó.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2),
(13, 2, 'Wiki', '<p>Te damos la bienvenida a la <a href="http://es.wikipedia.org/wiki/Wikipedia_en_espa%C3%B1ol" title="Wikipedia en español">Wikipedia en español</a>, un proyecto para construir una <a href="http://es.wikipedia.org/wiki/Wikipedia:La_enciclopedia_libre" title="Wikipedia:La enciclopedia libre">enciclopedia libre</a> que reúna todo el conocimiento humano en nuestro idioma, comenzado el 20 de mayo de <a href="http://es.wikipedia.org/wiki/2001" title="2001">2001</a> y que ya cuenta con <b>1&nbsp;018&nbsp;207 artículos</b>.</p>\r\n<p>Wikipedia crece cada día gracias a la participación de gente de todo \r\nel mundo. Es el mayor proyecto de recopilación de conocimiento jamás \r\nrealizado en la historia de la humanidad.</p>\r\n<p>Estamos encantados de haber llamado tu atención, y te invitamos a <a href="http://es.wikipedia.org/wiki/Wikipedia:Bienvenidos#C.C3.B3mo_puedes_colaborar">colaborar mejorando los artículos</a>\r\n y creando los que faltan. Todo el mundo es un pequeño o gran experto en\r\n algo; quizás te dediques a la enseñanza o a la investigación, o bien \r\ntengas acceso a información sobre la <a href="http://es.wikipedia.org/wiki/Historia" title="Historia">historia</a> de tu ciudad, o te encante el <a href="http://es.wikipedia.org/wiki/Ajedrez" title="Ajedrez">ajedrez</a>, o seas un gran fan de alguna <a href="http://es.wikipedia.org/wiki/Serie_de_televisi%C3%B3n" title="Serie de televisión">serie de televisión</a> o tipo de <a href="http://es.wikipedia.org/wiki/M%C3%BAsica" title="Música">música</a>. Hay miles de posibilidades; seas quien seas, <span style="color:#000;font-size: 130%;font-family:Times New Roman;">tú</span> puedes contribuir con tu saber en esta monumental obra.</p><p>Todo proyecto tiene sus <b>normas básicas</b> de funcionamiento. Estas son las nuestras:</p>\r\n<h3><span class="mw-headline" id="Los_cinco_pilares_de_Wikipedia">Los cinco pilares de Wikipedia</span> </h3>\r\n<div class="floatright"><a href="http://commons.wikimedia.org/wiki/File:Column_impost.svg" class="image"><img alt="Column impost.svg" src="http://upload.wikimedia.org/wikipedia/commons/thumb/5/57/Column_impost.svg/120px-Column_impost.svg.png" height="105" width="120"></a></div>\r\n<p>Los principios más fundamentales de Wikipedia son los denominados «<a href="http://es.wikipedia.org/wiki/WP:5P" title="WP:5P" class="mw-redirect">Cinco pilares</a>».</p>\r\n<ol><li><b>Wikipedia es una <a href="http://es.wikipedia.org/wiki/Enciclopedia" title="Enciclopedia">enciclopedia</a>, y todos los esfuerzos deben ir en ese sentido.</b></li><li><b>Todos los artículos deben estar redactados desde un <a href="http://es.wikipedia.org/wiki/Wikipedia:Punto_de_vista_neutral" title="Wikipedia:Punto de vista neutral">punto de vista neutral</a>.</b></li><li><b>El objetivo es construir una enciclopedia de <a href="http://es.wikipedia.org/wiki/Licencia_de_documentaci%C3%B3n_libre_de_GNU" title="Licencia de documentación libre de GNU">contenido libre</a>, por lo que en ningún caso se admite material con <a href="http://es.wikipedia.org/wiki/Wikipedia:Derechos_de_autor" title="Wikipedia:Derechos de autor">derechos de autor</a> (<i>copyrights</i>) sin el permiso correspondiente.</b></li><li><b>Wikipedia sigue unas <a href="http://es.wikipedia.org/wiki/Wikipedia:Etiqueta" title="Wikipedia:Etiqueta">normas de etiqueta</a> que deben respetarse.</b></li><li><b>Debes <a href="http://es.wikipedia.org/wiki/Wikipedia:S%C3%A9_valiente_editando_p%C3%A1ginas" title="Wikipedia:Sé valiente editando páginas">ser valiente editando páginas</a>, aunque siempre <a href="http://es.wikipedia.org/wiki/Wikipedia:Usa_el_sentido_com%C3%BAn" title="Wikipedia:Usa el sentido común">usando el sentido común</a>.</b></li></ol>Texto agregado<br><br>', '2013-06-03 08:56:30', '2013-06-03 08:56:30', 1),
(14, 2, 'El que llama a la Wiki', '<h3><span class="mw-headline" id="Normas_sobre_la_calidad">Normas sobre la calidad</span> </h3>\r\n<div class="floatright"><a href="http://commons.wikimedia.org/wiki/File:Crystal_Clear_action_run_approved.svg" class="image"><img alt="Crystal Clear action run approved.svg" src="http://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Crystal_Clear_action_run_approved.svg/120px-Crystal_Clear_action_run_approved.svg.png" height="109" width="120"></a></div>\r\n<p>Adicionalmente, es necesario contemplar otras tres reglas básicas indispensables para garantizar la calidad de los contenidos:</p>\r\n<ol><li>Wikipedia <b><a href="http://es.wikipedia.org/wiki/Wikipedia:Wikipedia_no_es_una_fuente_primaria" title="Wikipedia:Wikipedia no es una fuente primaria">no es fuente primaria</a></b>: la información nunca debe proceder en última instancia de los propios editores.</li><li><b><a href="http://es.wikipedia.org/wiki/Wikipedia:Verificabilidad" title="Wikipedia:Verificabilidad">Verificabilidad</a></b>: todos los artículos deben incluir <a href="http://es.wikipedia.org/wiki/Wikipedia:Referencias" title="Wikipedia:Referencias">referencias</a> a las fuentes de las que proviene la información.</li><li>Las fuentes de las que proviene la información deben ser <b><a href="http://es.wikipedia.org/wiki/Wikipedia:Fuentes_fiables" title="Wikipedia:Fuentes fiables">fuentes fiables</a></b>.</li></ol>\r\n<p>En otras palabras, es necesario prestar especial atención al contenido que se añade, que debe haber sido <a href="http://es.wikipedia.org/wiki/Wikipedia:Wikipedia_no_es_una_fuente_primaria" title="Wikipedia:Wikipedia no es una fuente primaria">publicado previamente</a> por un <a href="http://es.wikipedia.org/wiki/Wikipedia:Fuentes_fiables" title="Wikipedia:Fuentes fiables">autor de confianza</a>, y reflejar siempre <i>en el propio artículo</i> la <a href="http://es.wikipedia.org/wiki/Wikipedia:Verificabilidad" title="Wikipedia:Verificabilidad">fuente</a> de la que se obtuvo la información.</p>', '2013-06-03 08:57:32', '2013-06-03 08:57:32', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cambioestados`
--

CREATE TABLE IF NOT EXISTS `cambioestados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_estado` int(11) NOT NULL,
  `id_test` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `fechahora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_estado` (`id_estado`,`id_test`,`id_usuario`),
  KEY `id_test` (`id_test`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Modulo Test' AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `cambioestados`
--

INSERT INTO `cambioestados` (`id`, `id_estado`, `id_test`, `id_usuario`, `descripcion`, `fechahora`) VALUES
(1, 2, 1, 1, 'funciono', '2013-06-04 00:06:53'),
(2, 3, 1, 1, 'dddd', '2013-06-04 00:06:15'),
(3, 3, 1, 3, 'sadsfadf', '2013-06-04 00:06:50'),
(4, 1, 1, 3, 'asdfgs', '2013-06-04 00:06:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `complejidad`
--

CREATE TABLE IF NOT EXISTS `complejidad` (
  `idComplejidad` int(11) NOT NULL AUTO_INCREMENT,
  `descripcionComplejidad` varchar(100) NOT NULL,
  `puntos` int(11) NOT NULL,
  PRIMARY KEY (`idComplejidad`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `complejidad`
--

INSERT INTO `complejidad` (`idComplejidad`, `descripcionComplejidad`, `puntos`) VALUES
(1, 'Alta', 15),
(2, 'Media', 10),
(3, 'Baja', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `edm_cambios`
--

CREATE TABLE IF NOT EXISTS `edm_cambios` (
  `cambio_id` int(11) NOT NULL AUTO_INCREMENT,
  `cambio_documento` int(11) NOT NULL,
  `cambio_nombre` varchar(255) NOT NULL,
  `cambio_archivo` varchar(100) NOT NULL,
  `cambio_fechayhora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cambio_usuario` int(11) NOT NULL,
  `cambio_privilegios` int(11) NOT NULL,
  `cambio_descripcion` varchar(500) NOT NULL,
  `cambio_activo` int(1) NOT NULL,
  PRIMARY KEY (`cambio_id`),
  KEY `documento_usuario` (`cambio_usuario`),
  KEY `cambio_documento` (`cambio_documento`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `edm_cambios`
--

INSERT INTO `edm_cambios` (`cambio_id`, `cambio_documento`, `cambio_nombre`, `cambio_archivo`, `cambio_fechayhora`, `cambio_usuario`, `cambio_privilegios`, `cambio_descripcion`, `cambio_activo`) VALUES
(1, 1, 'ejemplo', 'Programa Programación Web Avanzada.pdf', '2013-06-03 21:24:55', 1, 1, 'descripción documento', 1),
(2, 1, 'ejemplo', 'edm.pdf', '2013-06-03 21:24:55', 1, 1, 'descripción documento descripción edm', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `edm_documento`
--

CREATE TABLE IF NOT EXISTS `edm_documento` (
  `documento_id` int(11) NOT NULL AUTO_INCREMENT,
  `documento_nombre` varchar(255) NOT NULL,
  `documento_archivo` varchar(100) NOT NULL,
  `documento_fechayhora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `documento_usuario` int(11) NOT NULL,
  `documento_privilegios` int(11) NOT NULL,
  `documento_descripcion` varchar(500) NOT NULL,
  `documento_activo` int(1) NOT NULL,
  `documento_proyecto` int(11) NOT NULL,
  PRIMARY KEY (`documento_id`),
  KEY `documento_usuario` (`documento_usuario`),
  KEY `documento_proyecto` (`documento_proyecto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `edm_documento`
--

INSERT INTO `edm_documento` (`documento_id`, `documento_nombre`, `documento_archivo`, `documento_fechayhora`, `documento_usuario`, `documento_privilegios`, `documento_descripcion`, `documento_activo`, `documento_proyecto`) VALUES
(1, 'ejemplo', 'edm.pdf', '2013-06-03 21:24:55', 1, 1, 'descripción documento descripción edm', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadolog`
--

CREATE TABLE IF NOT EXISTS `estadolog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `estadolog`
--

INSERT INTO `estadolog` (`id`, `descripcion`) VALUES
(1, 'Abierto'),
(2, 'Cerrado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `EstadoProyecto`
--

CREATE TABLE IF NOT EXISTS `EstadoProyecto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `EstadoProyecto` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `EstadoProyecto`
--

INSERT INTO `EstadoProyecto` (`id`, `EstadoProyecto`) VALUES
(1, 'Iniciado'),
(2, 'En Desarrollo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadorequerimiento`
--

CREATE TABLE IF NOT EXISTS `estadorequerimiento` (
  `idEstado` int(11) NOT NULL AUTO_INCREMENT,
  `DescripcionEstado` varchar(100) NOT NULL,
  PRIMARY KEY (`idEstado`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `estadorequerimiento`
--

INSERT INTO `estadorequerimiento` (`idEstado`, `DescripcionEstado`) VALUES
(1, 'En Proceso'),
(2, 'Finalizado'),
(3, 'Cancelado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE IF NOT EXISTS `estados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estado` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id`, `estado`) VALUES
(1, 'No Ejecutado'),
(2, 'Ejecutado OK'),
(3, 'Ejecutado Falla'),
(4, 'Dependiente'),
(5, 'No aplica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `etapaproyecto`
--

CREATE TABLE IF NOT EXISTS `etapaproyecto` (
  `idEtapa` int(11) NOT NULL AUTO_INCREMENT,
  `idProyecto` int(11) NOT NULL,
  `nombre` varchar(1000) NOT NULL,
  `descripcion` text NOT NULL,
  `fechaInicio` datetime NOT NULL,
  `fechaFin` datetime DEFAULT NULL,
  `porcentaje` double NOT NULL,
  PRIMARY KEY (`idEtapa`),
  KEY `idProyecto` (`idProyecto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `etapaproyecto`
--

INSERT INTO `etapaproyecto` (`idEtapa`, `idProyecto`, `nombre`, `descripcion`, `fechaInicio`, `fechaFin`, `porcentaje`) VALUES
(1, 2, 'etapa 1', 'inicio de etapa nro. 1sss', '2013-05-05 10:00:00', '2013-05-05 23:00:00', 37),
(2, 2, 'etapa 2', 'inicio etapa nro. 2', '2013-05-05 12:00:00', '2013-05-05 21:00:00', 4),
(3, 3, '5', '5dfasfd', '2013-06-12 00:00:00', NULL, 0),
(4, 4, 'ffffff', 'fffffff', '2013-06-19 00:00:00', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Funciones`
--

CREATE TABLE IF NOT EXISTS `Funciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Funcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `Funciones`
--

INSERT INTO `Funciones` (`id`, `Funcion`) VALUES
(1, 'Líder de Proyecto'),
(2, 'Programador'),
(3, 'Tester'),
(4, 'Analista Funcional'),
(5, 'Cliente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidentes`
--

CREATE TABLE IF NOT EXISTS `incidentes` (
  `idIncidente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `idProyecto` int(11) NOT NULL,
  `fechaAlta` datetime NOT NULL,
  `descripcion` text NOT NULL,
  `idSeveridad` int(11) NOT NULL,
  `idTipo` int(11) NOT NULL,
  PRIMARY KEY (`idIncidente`),
  KEY `idUsuario` (`idUsuario`),
  KEY `idProyecto` (`idProyecto`,`idSeveridad`,`idTipo`),
  KEY `idSeveridad` (`idSeveridad`),
  KEY `idTipo` (`idTipo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `incidentes`
--

INSERT INTO `incidentes` (`idIncidente`, `nombre`, `idUsuario`, `idProyecto`, `fechaAlta`, `descripcion`, `idSeveridad`, `idTipo`) VALUES
(1, 'listado de requerimientos', 1, 2, '2013-06-03 10:14:47', 'no se ve el nombre', 4, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `idLog` int(11) NOT NULL AUTO_INCREMENT,
  `idArticulo` int(10) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `fecha` datetime NOT NULL,
  `idUsuario` int(11) NOT NULL,
  PRIMARY KEY (`idLog`),
  KEY `idUsuario` (`idUsuario`),
  KEY `idArticulo` (`idArticulo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `log`
--

INSERT INTO `log` (`idLog`, `idArticulo`, `titulo`, `texto`, `fecha`, `idUsuario`) VALUES
(1, 8, 'actualizacion', 'La reforma judicial busca imponer control sobre los jueces para garantizar la impunidad de los funcionarios y amigos del poder involucrados en denuncias de corrupción, como el vicepresidente Amado Boudou, el ex secretario de Transporte Ricardo Jaime o el empresario Lázaro Báez", apuntó Stolbizer.', '0000-00-00 00:00:00', 1),
(2, 10, 'Nuev actualizacion ', 'la existencia de inflación reprimida y fuerte déficit fiscal vinculado a la necesidad de financiar los servicios públicos con tarifas congeladas y de cubrir el déficit energético, acentúa el riesgo de espiralización de la inflación, ', '0000-00-00 00:00:00', 1),
(3, 13, 'Wiki', '<p>Te damos la bienvenida a la <a href="http://es.wikipedia.org/wiki/Wikipedia_en_espa%C3%B1ol" title="Wikipedia en español">Wikipedia en español</a>, un proyecto para construir una <a href="http://es.wikipedia.org/wiki/Wikipedia:La_enciclopedia_libre" title="Wikipedia:La enciclopedia libre">enciclopedia libre</a> que reúna todo el conocimiento humano en nuestro idioma, comenzado el 20 de mayo de <a href="http://es.wikipedia.org/wiki/2001" title="2001">2001</a> y que ya cuenta con <b>1&nbsp;018&nbsp;207 artículos</b>.</p>\r\n<p>Wikipedia crece cada día gracias a la participación de gente de todo \r\nel mundo. Es el mayor proyecto de recopilación de conocimiento jamás \r\nrealizado en la historia de la humanidad.</p>\r\n<p>Estamos encantados de haber llamado tu atención, y te invitamos a <a href="http://es.wikipedia.org/wiki/Wikipedia:Bienvenidos#C.C3.B3mo_puedes_colaborar">colaborar mejorando los artículos</a>\r\n y creando los que faltan. Todo el mundo es un pequeño o gran experto en\r\n algo; quizás te dediques a la enseñanza o a la investigación, o bien \r\ntengas acceso a información sobre la <a href="http://es.wikipedia.org/wiki/Historia" title="Historia">historia</a> de tu ciudad, o te encante el <a href="http://es.wikipedia.org/wiki/Ajedrez" title="Ajedrez">ajedrez</a>, o seas un gran fan de alguna <a href="http://es.wikipedia.org/wiki/Serie_de_televisi%C3%B3n" title="Serie de televisión">serie de televisión</a> o tipo de <a href="http://es.wikipedia.org/wiki/M%C3%BAsica" title="Música">música</a>. Hay miles de posibilidades; seas quien seas, <span style="color:#000;font-size: 130%;font-family:Times New Roman;">tú</span> puedes contribuir con tu saber en esta monumental obra.</p><p>Todo proyecto tiene sus <b>normas básicas</b> de funcionamiento. Estas son las nuestras:</p>\r\n<h3><span class="mw-headline" id="Los_cinco_pilares_de_Wikipedia">Los cinco pilares de Wikipedia</span> </h3>\r\n<div class="floatright"><a href="http://commons.wikimedia.org/wiki/File:Column_impost.svg" class="image"><img alt="Column impost.svg" src="http://upload.wikimedia.org/wikipedia/commons/thumb/5/57/Column_impost.svg/120px-Column_impost.svg.png" height="105" width="120"></a></div>\r\n<p>Los principios más fundamentales de Wikipedia son los denominados «<a href="http://es.wikipedia.org/wiki/WP:5P" title="WP:5P" class="mw-redirect">Cinco pilares</a>».</p>\r\n<ol><li>Wikipedia es una <b><a href="http://es.wikipedia.org/wiki/Enciclopedia" title="Enciclopedia">enciclopedia</a></b>, y todos los esfuerzos deben ir en ese sentido.</li><li>Todos los artículos deben estar redactados desde un <b><a href="http://es.wikipedia.org/wiki/Wikipedia:Punto_de_vista_neutral" title="Wikipedia:Punto de vista neutral">punto de vista neutral</a></b>.</li><li>El objetivo es construir una enciclopedia de <b><a href="http://es.wikipedia.org/wiki/Licencia_de_documentaci%C3%B3n_libre_de_GNU" title="Licencia de documentación libre de GNU">contenido libre</a></b>, por lo que en ningún caso se admite material con <a href="http://es.wikipedia.org/wiki/Wikipedia:Derechos_de_autor" title="Wikipedia:Derechos de autor">derechos de autor</a> (<i>copyrights</i>) sin el permiso correspondiente.</li><li>Wikipedia sigue unas <b><a href="http://es.wikipedia.org/wiki/Wikipedia:Etiqueta" title="Wikipedia:Etiqueta">normas de etiqueta</a></b> que deben respetarse.</li><li>Debes <a href="http://es.wikipedia.org/wiki/Wikipedia:S%C3%A9_valiente_editando_p%C3%A1ginas" title="Wikipedia:Sé valiente editando páginas">ser valiente editando páginas</a>, aunque siempre <b><a href="http://es.wikipedia.org/wiki/Wikipedia:Usa_el_sentido_com%C3%BAn" title="Wikipedia:Usa el sentido común">usando el sentido común</a></b>.</li></ol>Texto agregado<br><br>', '0000-00-00 00:00:00', 1),
(4, 13, 'Wiki', '<p>Te damos la bienvenida a la <a href="http://es.wikipedia.org/wiki/Wikipedia_en_espa%C3%B1ol" title="Wikipedia en español">Wikipedia en español</a>, un proyecto para construir una <a href="http://es.wikipedia.org/wiki/Wikipedia:La_enciclopedia_libre" title="Wikipedia:La enciclopedia libre">enciclopedia libre</a> que reúna todo el conocimiento humano en nuestro idioma, comenzado el 20 de mayo de <a href="http://es.wikipedia.org/wiki/2001" title="2001">2001</a> y que ya cuenta con <b>1&nbsp;018&nbsp;207 artículos</b>.</p>\r\n<p>Wikipedia crece cada día gracias a la participación de gente de todo \r\nel mundo. Es el mayor proyecto de recopilación de conocimiento jamás \r\nrealizado en la historia de la humanidad.</p>\r\n<p>Estamos encantados de haber llamado tu atención, y te invitamos a <a href="http://es.wikipedia.org/wiki/Wikipedia:Bienvenidos#C.C3.B3mo_puedes_colaborar">colaborar mejorando los artículos</a>\r\n y creando los que faltan. Todo el mundo es un pequeño o gran experto en\r\n algo; quizás te dediques a la enseñanza o a la investigación, o bien \r\ntengas acceso a información sobre la <a href="http://es.wikipedia.org/wiki/Historia" title="Historia">historia</a> de tu ciudad, o te encante el <a href="http://es.wikipedia.org/wiki/Ajedrez" title="Ajedrez">ajedrez</a>, o seas un gran fan de alguna <a href="http://es.wikipedia.org/wiki/Serie_de_televisi%C3%B3n" title="Serie de televisión">serie de televisión</a> o tipo de <a href="http://es.wikipedia.org/wiki/M%C3%BAsica" title="Música">música</a>. Hay miles de posibilidades; seas quien seas, <span style="color:#000;font-size: 130%;font-family:Times New Roman;">tú</span> puedes contribuir con tu saber en esta monumental obra.</p><p>Todo proyecto tiene sus <b>normas básicas</b> de funcionamiento. Estas son las nuestras:</p>\r\n<h3><span class="mw-headline" id="Los_cinco_pilares_de_Wikipedia">Los cinco pilares de Wikipedia</span> </h3>\r\n<div class="floatright"><a href="http://commons.wikimedia.org/wiki/File:Column_impost.svg" class="image"><img alt="Column impost.svg" src="http://upload.wikimedia.org/wikipedia/commons/thumb/5/57/Column_impost.svg/120px-Column_impost.svg.png" height="105" width="120"></a></div>\r\n<p>Los principios más fundamentales de Wikipedia son los denominados «<a href="http://es.wikipedia.org/wiki/WP:5P" title="WP:5P" class="mw-redirect">Cinco pilares</a>».</p>\r\n<ol><li><b>Wikipedia es una <a href="http://es.wikipedia.org/wiki/Enciclopedia" title="Enciclopedia">enciclopedia</a>, y todos los esfuerzos deben ir en ese sentido.</b></li><li><b>Todos los artículos deben estar redactados desde un <a href="http://es.wikipedia.org/wiki/Wikipedia:Punto_de_vista_neutral" title="Wikipedia:Punto de vista neutral">punto de vista neutral</a>.</b></li><li><b>El objetivo es construir una enciclopedia de <a href="http://es.wikipedia.org/wiki/Licencia_de_documentaci%C3%B3n_libre_de_GNU" title="Licencia de documentación libre de GNU">contenido libre</a>, por lo que en ningún caso se admite material con <a href="http://es.wikipedia.org/wiki/Wikipedia:Derechos_de_autor" title="Wikipedia:Derechos de autor">derechos de autor</a> (<i>copyrights</i>) sin el permiso correspondiente.</b></li><li><b>Wikipedia sigue unas <a href="http://es.wikipedia.org/wiki/Wikipedia:Etiqueta" title="Wikipedia:Etiqueta">normas de etiqueta</a> que deben respetarse.</b></li><li><b>Debes <a href="http://es.wikipedia.org/wiki/Wikipedia:S%C3%A9_valiente_editando_p%C3%A1ginas" title="Wikipedia:Sé valiente editando páginas">ser valiente editando páginas</a>, aunque siempre <a href="http://es.wikipedia.org/wiki/Wikipedia:Usa_el_sentido_com%C3%BAn" title="Wikipedia:Usa el sentido común">usando el sentido común</a>.</b></li></ol>Texto agregado<br><br>', '0000-00-00 00:00:00', 1),
(5, 13, 'Wiki', '<p>Te damos la bienvenida a la <a href="http://es.wikipedia.org/wiki/Wikipedia_en_espa%C3%B1ol" title="Wikipedia en español">Wikipedia en español</a>, un proyecto para construir una <a href="http://es.wikipedia.org/wiki/Wikipedia:La_enciclopedia_libre" title="Wikipedia:La enciclopedia libre">enciclopedia libre</a> que reúna todo el conocimiento humano en nuestro idioma, comenzado el 20 de mayo de <a href="http://es.wikipedia.org/wiki/2001" title="2001">2001</a> y que ya cuenta con <b>1&nbsp;018&nbsp;207 artículos</b>.</p>\r\n<p>Wikipedia crece cada día gracias a la participación de gente de todo \r\nel mundo. Es el mayor proyecto de recopilación de conocimiento jamás \r\nrealizado en la historia de la humanidad.</p>\r\n<p>Estamos encantados de haber llamado tu atención, y te invitamos a <a href="http://es.wikipedia.org/wiki/Wikipedia:Bienvenidos#C.C3.B3mo_puedes_colaborar">colaborar mejorando los artículos</a>\r\n y creando los que faltan. Todo el mundo es un pequeño o gran experto en\r\n algo; quizás te dediques a la enseñanza o a la investigación, o bien \r\ntengas acceso a información sobre la <a href="http://es.wikipedia.org/wiki/Historia" title="Historia">historia</a> de tu ciudad, o te encante el <a href="http://es.wikipedia.org/wiki/Ajedrez" title="Ajedrez">ajedrez</a>, o seas un gran fan de alguna <a href="http://es.wikipedia.org/wiki/Serie_de_televisi%C3%B3n" title="Serie de televisión">serie de televisión</a> o tipo de <a href="http://es.wikipedia.org/wiki/M%C3%BAsica" title="Música">música</a>. Hay miles de posibilidades; seas quien seas, <span style="color:#000;font-size: 130%;font-family:Times New Roman;">tú</span> puedes contribuir con tu saber en esta monumental obra.</p><p>Todo proyecto tiene sus <b>normas básicas</b> de funcionamiento. Estas son las nuestras:</p>\r\n<h3><span class="mw-headline" id="Los_cinco_pilares_de_Wikipedia">Los cinco pilares de Wikipedia</span> </h3>\r\n<div class="floatright"><a href="http://commons.wikimedia.org/wiki/File:Column_impost.svg" class="image"><img alt="Column impost.svg" src="http://upload.wikimedia.org/wikipedia/commons/thumb/5/57/Column_impost.svg/120px-Column_impost.svg.png" height="105" width="120"></a></div>\r\n<p>Los principios más fundamentales de Wikipedia son los denominados «<a href="http://es.wikipedia.org/wiki/WP:5P" title="WP:5P" class="mw-redirect">Cinco pilares</a>».</p>\r\n<ol><li><b>Wikipedia es una <a href="http://es.wikipedia.org/wiki/Enciclopedia" title="Enciclopedia">enciclopedia</a>, y todos los esfuerzos deben ir en ese sentido.</b></li><li><b>Todos los artículos deben estar redactados desde un <a href="http://es.wikipedia.org/wiki/Wikipedia:Punto_de_vista_neutral" title="Wikipedia:Punto de vista neutral">punto de vista neutral</a>.</b></li><li><b>El objetivo es construir una enciclopedia de <a href="http://es.wikipedia.org/wiki/Licencia_de_documentaci%C3%B3n_libre_de_GNU" title="Licencia de documentación libre de GNU">contenido libre</a>, por lo que en ningún caso se admite material con <a href="http://es.wikipedia.org/wiki/Wikipedia:Derechos_de_autor" title="Wikipedia:Derechos de autor">derechos de autor</a> (<i>copyrights</i>) sin el permiso correspondiente.</b></li><li><b>Wikipedia sigue unas <a href="http://es.wikipedia.org/wiki/Wikipedia:Etiqueta" title="Wikipedia:Etiqueta">normas de etiqueta</a> que deben respetarse.</b></li><li><b>Debes <a href="http://es.wikipedia.org/wiki/Wikipedia:S%C3%A9_valiente_editando_p%C3%A1ginas" title="Wikipedia:Sé valiente editando páginas">ser valiente editando páginas</a>, aunque siempre <a href="http://es.wikipedia.org/wiki/Wikipedia:Usa_el_sentido_com%C3%BAn" title="Wikipedia:Usa el sentido común">usando el sentido común</a>.</b></li></ol>Texto agregado<br><br>', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logincidentes`
--

CREATE TABLE IF NOT EXISTS `logincidentes` (
  `idLog` int(11) NOT NULL AUTO_INCREMENT,
  `idIncidente` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `idEstado` int(11) NOT NULL,
  PRIMARY KEY (`idLog`),
  KEY `idIncidente` (`idIncidente`),
  KEY `idUsuario` (`idUsuario`),
  KEY `idEstado` (`idEstado`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `logincidentes`
--

INSERT INTO `logincidentes` (`idLog`, `idIncidente`, `fecha`, `idUsuario`, `descripcion`, `idEstado`) VALUES
(1, 1, '2013-06-03 10:16:44', 1, 'Tomo el incidente y lo chequeo\r\n', 1),
(2, 1, '2013-06-03 10:17:45', 3, 'Solucionado', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Proyecto`
--

CREATE TABLE IF NOT EXISTS `Proyecto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Proyecto` varchar(1000) NOT NULL,
  `Descripcion` text NOT NULL,
  `FechaIncicio` datetime NOT NULL,
  `FechaFin` datetime DEFAULT NULL,
  `idEstado` int(11) NOT NULL,
  `ValorHora` double NOT NULL,
  `HorasSemanales` int(11) NOT NULL,
  `TiempoAjustadoPuntos` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`,`idEstado`),
  KEY `Proyecto_ibfk_1` (`idEstado`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `Proyecto`
--

INSERT INTO `Proyecto` (`id`, `Proyecto`, `Descripcion`, `FechaIncicio`, `FechaFin`, `idEstado`, `ValorHora`, `HorasSemanales`, `TiempoAjustadoPuntos`) VALUES
(2, 'Proyecto 1', 'ldldl a fdaf', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 150, 15, 0.5),
(3, 'Proyecto 2', 'dddddddddd', '2012-01-01 00:00:00', '0000-00-00 00:00:00', 1, 1, 1, 1),
(4, 'Proyecto 3', 'Proyecto3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, 1, 1),
(5, 'Cliente', 'ldlld', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recursoproyecto`
--

CREATE TABLE IF NOT EXISTS `recursoproyecto` (
  `idEtapa` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  PRIMARY KEY (`idEtapa`,`idUsuario`),
  KEY `idEtapa` (`idEtapa`,`idUsuario`),
  KEY `idUsuario` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `recursoproyecto`
--

INSERT INTO `recursoproyecto` (`idEtapa`, `idUsuario`) VALUES
(1, 1),
(2, 1),
(1, 2),
(2, 2),
(3, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `requerimientos`
--

CREATE TABLE IF NOT EXISTS `requerimientos` (
  `idRequerimientos` int(11) NOT NULL AUTO_INCREMENT,
  `nombreRequerimiento` varchar(100) NOT NULL,
  `actores` varchar(100) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `costoReal` float NOT NULL,
  `datosUtilizados` varchar(100) DEFAULT NULL,
  `idComplejidadRequerimiento` int(11) DEFAULT NULL,
  `idEstadoRequerimiento` int(11) DEFAULT NULL,
  `idUsuarioRequerimiento` int(11) DEFAULT NULL,
  `idProyectoRequerimiento` int(11) DEFAULT NULL,
  PRIMARY KEY (`idRequerimientos`),
  KEY `idProyectoRequerimiento` (`idProyectoRequerimiento`),
  KEY `idUsuarioRequerimiento` (`idUsuarioRequerimiento`),
  KEY `idEstadoRequerimiento` (`idEstadoRequerimiento`),
  KEY `idComplejidadRequerimiento` (`idComplejidadRequerimiento`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `requerimientos`
--

INSERT INTO `requerimientos` (`idRequerimientos`, `nombreRequerimiento`, `actores`, `descripcion`, `costoReal`, `datosUtilizados`, `idComplejidadRequerimiento`, `idEstadoRequerimiento`, `idUsuarioRequerimiento`, `idProyectoRequerimiento`) VALUES
(1, 'Listar Requerimientos', 'ddd', 'Listar Requerimientos', 6, 'ddd', 3, 2, 1, 2),
(2, 'Estadistica de Proyecto', NULL, 'Estadistica de Proyecto', 24, NULL, 1, 1, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Rol`
--

CREATE TABLE IF NOT EXISTS `Rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Rol` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `Rol`
--

INSERT INTO `Rol` (`id`, `Rol`) VALUES
(1, 'administrador'),
(2, 'empleado'),
(3, 'cliente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rrhh_dedicacion`
--

CREATE TABLE IF NOT EXISTS `rrhh_dedicacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idEtapa` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `dedicacion` varchar(45) NOT NULL,
  `hora` double NOT NULL,
  `fecha` date NOT NULL,
  `descripcion` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idEtapa` (`idEtapa`),
  KEY `idUsuario` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `severidadincidente`
--

CREATE TABLE IF NOT EXISTS `severidadincidente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `severidad` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `severidadincidente`
--

INSERT INTO `severidadincidente` (`id`, `severidad`) VALUES
(3, ''),
(4, 'Alta'),
(5, 'Baja'),
(6, 'Media');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_proyecto` int(11) NOT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  `resultadoesperado` varchar(250) DEFAULT NULL,
  `pasos` varchar(250) DEFAULT NULL,
  `prioridad` varchar(25) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `fechahora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_usuario_registro` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_proyecto` (`id_proyecto`,`id_tipo`,`id_usuario_registro`),
  KEY `id_tipo` (`id_tipo`),
  KEY `id_usuario_registro` (`id_usuario_registro`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Modulo Test' AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `test`
--

INSERT INTO `test` (`id`, `id_proyecto`, `descripcion`, `resultadoesperado`, `pasos`, `prioridad`, `id_tipo`, `fechahora`, `id_usuario_registro`) VALUES
(1, 2, 'Listado de Requerimientos', 'ver requerimientosdd', 'seleccionar proyecto y ver requerimientos', 'alta', 1, '2013-06-04 00:06:43', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoincidente`
--

CREATE TABLE IF NOT EXISTS `tipoincidente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `tipoincidente`
--

INSERT INTO `tipoincidente` (`id`, `tipo`) VALUES
(1, 'Bugs'),
(2, 'Errores'),
(3, 'Cambios'),
(4, 'Mejoras');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipotest`
--

CREATE TABLE IF NOT EXISTS `tipotest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Modulo Test' AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `tipotest`
--

INSERT INTO `tipotest` (`id`, `tipo`) VALUES
(1, 'Funcional'),
(2, 'No Funcional');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Usuario`
--

CREATE TABLE IF NOT EXISTS `Usuario` (
  `Usuario` varchar(255) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Clave` varchar(32) NOT NULL,
  `idRol` int(11) NOT NULL,
  `NombreApellido` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Usuario` (`Usuario`),
  KEY `idRol` (`idRol`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `Usuario`
--

INSERT INTO `Usuario` (`Usuario`, `id`, `Clave`, `idRol`, `NombreApellido`, `Email`) VALUES
('admin', 1, 'admin', 1, 'Administra Administrador', 'admin@tormenta.com'),
('desarrollador', 2, 'desarrollador', 2, 'Emplea Empleado', 'empleado@tormenta.com'),
('cliente', 3, 'cliente', 3, 'Clie Cliente', 'cliente@empresacliente.com'),
('usu1', 4, 'usu1', 2, 'Usu Uno', 'usu@tengo.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `UsuarioProyectoFuncion`
--

CREATE TABLE IF NOT EXISTS `UsuarioProyectoFuncion` (
  `idUsuario` int(11) NOT NULL,
  `idProyecto` int(11) NOT NULL,
  `idFuncion` int(11) NOT NULL,
  PRIMARY KEY (`idUsuario`,`idProyecto`,`idFuncion`),
  KEY `idFuncion` (`idFuncion`),
  KEY `idProyecto` (`idProyecto`),
  KEY `idUsuario` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `UsuarioProyectoFuncion`
--

INSERT INTO `UsuarioProyectoFuncion` (`idUsuario`, `idProyecto`, `idFuncion`) VALUES
(1, 2, 1),
(1, 3, 1),
(2, 2, 1),
(2, 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariosasignados`
--

CREATE TABLE IF NOT EXISTS `usuariosasignados` (
  `idUsuario` int(11) NOT NULL,
  `idRequerimiento` int(11) NOT NULL,
  PRIMARY KEY (`idUsuario`,`idRequerimiento`),
  KEY `idUsuario` (`idUsuario`,`idRequerimiento`),
  KEY `idRequerimiento` (`idRequerimiento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuariosasignados`
--

INSERT INTO `usuariosasignados` (`idUsuario`, `idRequerimiento`) VALUES
(1, 1);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `articulo`
--
ALTER TABLE `articulo`
  ADD CONSTRAINT `articulo_ibfk_1` FOREIGN KEY (`idProyecto`) REFERENCES `Proyecto` (`id`),
  ADD CONSTRAINT `articulo_ibfk_2` FOREIGN KEY (`idUsuario`) REFERENCES `Usuario` (`id`);

--
-- Filtros para la tabla `cambioestados`
--
ALTER TABLE `cambioestados`
  ADD CONSTRAINT `cambioestados_ibfk_1` FOREIGN KEY (`id_test`) REFERENCES `test` (`id`),
  ADD CONSTRAINT `cambioestados_ibfk_2` FOREIGN KEY (`id_estado`) REFERENCES `estados` (`id`),
  ADD CONSTRAINT `cambioestados_ibfk_3` FOREIGN KEY (`id_usuario`) REFERENCES `Usuario` (`id`);

--
-- Filtros para la tabla `edm_cambios`
--
ALTER TABLE `edm_cambios`
  ADD CONSTRAINT `edm_cambios_ibfk_1` FOREIGN KEY (`cambio_documento`) REFERENCES `edm_documento` (`documento_id`),
  ADD CONSTRAINT `edm_cambios_ibfk_2` FOREIGN KEY (`cambio_usuario`) REFERENCES `Usuario` (`id`);

--
-- Filtros para la tabla `edm_documento`
--
ALTER TABLE `edm_documento`
  ADD CONSTRAINT `edm_documento_ibfk_1` FOREIGN KEY (`documento_proyecto`) REFERENCES `Proyecto` (`id`),
  ADD CONSTRAINT `edm_documento_ibfk_2` FOREIGN KEY (`documento_usuario`) REFERENCES `Usuario` (`id`);

--
-- Filtros para la tabla `etapaproyecto`
--
ALTER TABLE `etapaproyecto`
  ADD CONSTRAINT `etapaproyecto_ibfk_1` FOREIGN KEY (`idProyecto`) REFERENCES `Proyecto` (`id`);

--
-- Filtros para la tabla `incidentes`
--
ALTER TABLE `incidentes`
  ADD CONSTRAINT `incidentes_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `Usuario` (`id`),
  ADD CONSTRAINT `incidentes_ibfk_2` FOREIGN KEY (`idProyecto`) REFERENCES `Proyecto` (`id`),
  ADD CONSTRAINT `incidentes_ibfk_3` FOREIGN KEY (`idSeveridad`) REFERENCES `severidadincidente` (`id`),
  ADD CONSTRAINT `incidentes_ibfk_4` FOREIGN KEY (`idTipo`) REFERENCES `tipoincidente` (`id`);

--
-- Filtros para la tabla `log`
--
ALTER TABLE `log`
  ADD CONSTRAINT `log_ibfk_1` FOREIGN KEY (`idArticulo`) REFERENCES `articulo` (`idArticulo`),
  ADD CONSTRAINT `log_ibfk_2` FOREIGN KEY (`idUsuario`) REFERENCES `Usuario` (`id`);

--
-- Filtros para la tabla `logincidentes`
--
ALTER TABLE `logincidentes`
  ADD CONSTRAINT `logincidentes_ibfk_1` FOREIGN KEY (`idIncidente`) REFERENCES `incidentes` (`idIncidente`),
  ADD CONSTRAINT `logincidentes_ibfk_2` FOREIGN KEY (`idUsuario`) REFERENCES `Usuario` (`id`),
  ADD CONSTRAINT `logincidentes_ibfk_3` FOREIGN KEY (`idEstado`) REFERENCES `estadolog` (`id`);

--
-- Filtros para la tabla `Proyecto`
--
ALTER TABLE `Proyecto`
  ADD CONSTRAINT `Proyecto_ibfk_1` FOREIGN KEY (`idEstado`) REFERENCES `EstadoProyecto` (`id`);

--
-- Filtros para la tabla `recursoproyecto`
--
ALTER TABLE `recursoproyecto`
  ADD CONSTRAINT `recursoproyecto_ibfk_1` FOREIGN KEY (`idEtapa`) REFERENCES `etapaproyecto` (`idEtapa`),
  ADD CONSTRAINT `recursoproyecto_ibfk_2` FOREIGN KEY (`idUsuario`) REFERENCES `Usuario` (`id`);

--
-- Filtros para la tabla `requerimientos`
--
ALTER TABLE `requerimientos`
  ADD CONSTRAINT `requerimientos_ibfk_1` FOREIGN KEY (`idProyectoRequerimiento`) REFERENCES `Proyecto` (`id`),
  ADD CONSTRAINT `requerimientos_ibfk_2` FOREIGN KEY (`idEstadoRequerimiento`) REFERENCES `estadorequerimiento` (`idEstado`),
  ADD CONSTRAINT `requerimientos_ibfk_3` FOREIGN KEY (`idUsuarioRequerimiento`) REFERENCES `Usuario` (`id`),
  ADD CONSTRAINT `requerimientos_ibfk_4` FOREIGN KEY (`idComplejidadRequerimiento`) REFERENCES `complejidad` (`idComplejidad`);

--
-- Filtros para la tabla `rrhh_dedicacion`
--
ALTER TABLE `rrhh_dedicacion`
  ADD CONSTRAINT `rrhh_dedicacion_ibfk_2` FOREIGN KEY (`idUsuario`) REFERENCES `Usuario` (`id`),
  ADD CONSTRAINT `rrhh_dedicacion_ibfk_1` FOREIGN KEY (`idEtapa`) REFERENCES `etapaproyecto` (`idEtapa`);

--
-- Filtros para la tabla `test`
--
ALTER TABLE `test`
  ADD CONSTRAINT `test_ibfk_1` FOREIGN KEY (`id_proyecto`) REFERENCES `Proyecto` (`id`),
  ADD CONSTRAINT `test_ibfk_2` FOREIGN KEY (`id_tipo`) REFERENCES `tipotest` (`id`),
  ADD CONSTRAINT `test_ibfk_3` FOREIGN KEY (`id_usuario_registro`) REFERENCES `Usuario` (`id`);

--
-- Filtros para la tabla `Usuario`
--
ALTER TABLE `Usuario`
  ADD CONSTRAINT `Usuario_ibfk_1` FOREIGN KEY (`idRol`) REFERENCES `Rol` (`id`);

--
-- Filtros para la tabla `UsuarioProyectoFuncion`
--
ALTER TABLE `UsuarioProyectoFuncion`
  ADD CONSTRAINT `UsuarioProyectoFuncion_ibfk_1` FOREIGN KEY (`idProyecto`) REFERENCES `Proyecto` (`id`),
  ADD CONSTRAINT `UsuarioProyectoFuncion_ibfk_2` FOREIGN KEY (`idUsuario`) REFERENCES `Usuario` (`id`),
  ADD CONSTRAINT `UsuarioProyectoFuncion_ibfk_3` FOREIGN KEY (`idFuncion`) REFERENCES `Funciones` (`id`);

--
-- Filtros para la tabla `usuariosasignados`
--
ALTER TABLE `usuariosasignados`
  ADD CONSTRAINT `usuariosasignados_ibfk_1` FOREIGN KEY (`idRequerimiento`) REFERENCES `requerimientos` (`idRequerimientos`),
  ADD CONSTRAINT `usuariosasignados_ibfk_2` FOREIGN KEY (`idUsuario`) REFERENCES `Usuario` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;