<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright CopyLeft
 * @license GPL
 */

namespace app\widgets;


use yii\bootstrap\Nav;
use yii\base\Widget;

/**
 * Nav renders a nav HTML component.
 *
 * For example:
 *
 * ```php
 * echo NuestroNav::widget(['idProyecto'=>$idProyecto]);
 * ```
 * @author Modulo Wiki
 * @since 2.0
 */
class NuestroNav extends Widget
{    /**
     * @var int Id del proyecto para armar el Nav 
     */
    public $idProyecto = null;
	public function run()
	{
		if (is_null($this->idProyecto)){
			throw new Exception ('Error: Falta idProyecto');
		}
		$id= $this->idProyecto;
		$items=[
		['label' => 'edm',
		'url' => ['/edm','idProyecto'=>$id],
		],
		['label' => 'wiki',
		'url' => ['/wiki','idProyecto'=>$id],
		],
		['label' => 'rrhh',
		'url' => ['/rrhh','idProyecto'=>$id],
		],
		['label' => 'requerimientos',
		'url' => ['/requerimientos','idProyecto'=>$id],
		],
		['label' => 'metricas',
		'url' => ['/metricas','idProyecto'=>$id],
		],
		['label' => 'issue',
		'url' => ['/issue','idProyecto'=>$id],
		],
		['label' => 'test',
		'url' => ['/test','idProyecto'=>$id],
		],
		['label' => 'tareas',
		'url' => ['/tareas','idProyecto'=>$id],
		],
		];
		return Nav::widget([
				'options' => ['class' => 'navbar-nav navbar-right'],
				'items' => $items,
				]);
	}
}
