<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "complejidad".
 *
 * @property integer $idComplejidad
 * @property string $descripcionComplejidad
 * @property integer $puntos
 *
 * @property Requerimientos[] $requerimientos
 */
class Complejidad extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'complejidad';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descripcionComplejidad', 'puntos'], 'required'],
            [['puntos'], 'integer'],
            [['descripcionComplejidad'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idComplejidad' => 'Id Complejidad',
            'descripcionComplejidad' => 'Descripcion Complejidad',
            'puntos' => 'Puntos',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequerimientos()
    {
        return $this->hasMany(Requerimientos::className(), ['idComplejidadRequerimiento' => 'idComplejidad']);
    }
}
