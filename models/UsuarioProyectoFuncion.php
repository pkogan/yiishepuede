<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "UsuarioProyectoFuncion".
 *
 * @property integer $idUsuario
 * @property integer $idProyecto
 * @property integer $idFuncion
 *
 * @property Proyecto $idProyecto0
 * @property Usuario $idUsuario0
 * @property Funciones $idFuncion0
 */
class UsuarioProyectoFuncion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'UsuarioProyectoFuncion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idUsuario', 'idProyecto', 'idFuncion'], 'required'],
            [['idUsuario', 'idProyecto', 'idFuncion'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idUsuario' => 'Id Usuario',
            'idProyecto' => 'Id Proyecto',
            'idFuncion' => 'Id Funcion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProyecto0()
    {
        return $this->hasOne(Proyecto::className(), ['id' => 'idProyecto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuario0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idUsuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdFuncion0()
    {
        return $this->hasOne(Funciones::className(), ['id' => 'idFuncion']);
    }
}
