<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Funciones;

/**
 * FuncionesSearch represents the model behind the search form about `app\models\Funciones`.
 */
class FuncionesSearch extends Funciones
{
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['Funcion'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Funciones::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'Funcion', $this->Funcion]);

        return $dataProvider;
    }
}
