<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "edm_cambios".
 *
 * @property integer $cambio_id
 * @property integer $cambio_documento
 * @property string $cambio_nombre
 * @property string $cambio_archivo
 * @property string $cambio_fechayhora
 * @property integer $cambio_usuario
 * @property integer $cambio_privilegios
 * @property string $cambio_descripcion
 * @property integer $cambio_activo
 *
 * @property EdmDocumento $cambioDocumento
 * @property Usuario $cambioUsuario
 */
class EdmCambios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'edm_cambios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cambio_documento', 'cambio_nombre', 'cambio_archivo', 'cambio_fechayhora', 'cambio_usuario', 'cambio_privilegios', 'cambio_descripcion', 'cambio_activo'], 'required'],
            [['cambio_documento', 'cambio_usuario', 'cambio_privilegios', 'cambio_activo'], 'integer'],
            [['cambio_fechayhora'], 'safe'],
            [['cambio_nombre'], 'string', 'max' => 255],
            [['cambio_archivo'], 'string', 'max' => 100],
            [['cambio_descripcion'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cambio_id' => 'Cambio ID',
            'cambio_documento' => 'Cambio Documento',
            'cambio_nombre' => 'Cambio Nombre',
            'cambio_archivo' => 'Cambio Archivo',
            'cambio_fechayhora' => 'Cambio Fechayhora',
            'cambio_usuario' => 'Cambio Usuario',
            'cambio_privilegios' => 'Cambio Privilegios',
            'cambio_descripcion' => 'Cambio Descripcion',
            'cambio_activo' => 'Cambio Activo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCambioDocumento()
    {
        return $this->hasOne(EdmDocumento::className(), ['documento_id' => 'cambio_documento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCambioUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'cambio_usuario']);
    }
}
