<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UsuarioProyectoFuncion;

/**
 * UsuarioProyectoFuncionSearch represents the model behind the search form about `app\models\UsuarioProyectoFuncion`.
 */
class UsuarioProyectoFuncionSearch extends UsuarioProyectoFuncion
{
    public function rules()
    {
        return [
            [['idUsuario', 'idProyecto', 'idFuncion'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params=null)
    {
        $query = UsuarioProyectoFuncion::find();
        $query->joinWith(['idUsuario0','idFuncion0']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idUsuario' => $this->idUsuario,
            'idProyecto' => $this->idProyecto,
            'idFuncion' => $this->idFuncion,
        ]);

        return $dataProvider;
    }
}
