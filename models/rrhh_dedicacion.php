<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rrhh_dedicacion".
 *
 * @property integer $id
 * @property integer $idEtapa
 * @property integer $idUsuario
 * @property string $dedicacion
 * @property double $hora
 * @property string $fecha
 * @property string $descripcion
 *
 * @property Etapaproyecto $idEtapa0
 * @property Usuario $idUsuario0
 */
class rrhh_dedicacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rrhh_dedicacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idEtapa', 'idUsuario', 'dedicacion', 'hora', 'fecha', 'descripcion'], 'required'],
            [['idEtapa', 'idUsuario'], 'integer'],
            [['hora'], 'number'],
            [['fecha'], 'safe'],
            [['dedicacion'], 'string', 'max' => 45],
            [['descripcion'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idEtapa' => 'Etapa Proyecto',
            'idUsuario' => 'Usuario',
            'dedicacion' => 'Dedicacion',
            'hora' => 'Hora',
            'fecha' => 'Fecha',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEtapa0()
    {
        return $this->hasOne(Etapaproyecto::className(), ['idEtapa' => 'idEtapa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuario0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idUsuario']);
    }
}
