<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "edm_documento".
 *
 * @property integer $documento_id
 * @property string $documento_nombre
 * @property string $documento_archivo
 * @property string $documento_fechayhora
 * @property integer $documento_usuario
 * @property integer $documento_privilegios
 * @property string $documento_descripcion
 * @property integer $documento_activo
 * @property integer $documento_proyecto
 *
 * @property EdmCambios[] $edmCambios
 * @property Proyecto $documentoProyecto
 * @property Usuario $documentoUsuario
 */
class EdmDocumento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'edm_documento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['documento_nombre', 'documento_archivo', 'documento_fechayhora', 'documento_usuario', 'documento_privilegios', 'documento_descripcion', 'documento_activo', 'documento_proyecto'], 'required'],
            [['documento_fechayhora'], 'safe'],
            [['documento_usuario', 'documento_privilegios', 'documento_activo', 'documento_proyecto'], 'integer'],
            [['documento_nombre'], 'string', 'max' => 255],
            [['documento_archivo'], 'string', 'max' => 100],
            [['documento_descripcion'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'documento_id' => 'Documento ID',
            'documento_nombre' => 'Documento Nombre',
            'documento_archivo' => 'Documento Archivo',
            'documento_fechayhora' => 'Documento Fechayhora',
            'documento_usuario' => 'Documento Usuario',
            'documento_privilegios' => 'Documento Privilegios',
            'documento_descripcion' => 'Documento Descripcion',
            'documento_activo' => 'Documento Activo',
            'documento_proyecto' => 'Documento Proyecto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEdmCambios()
    {
        return $this->hasMany(EdmCambios::className(), ['cambio_documento' => 'documento_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentoProyecto()
    {
        return $this->hasOne(Proyecto::className(), ['id' => 'documento_proyecto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentoUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'documento_usuario']);
    }
}
