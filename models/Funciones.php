<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Funciones".
 *
 * @property integer $id
 * @property string $Funcion
 *
 * @property UsuarioProyectoFuncion $usuarioProyectoFuncion
 */
class Funciones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Funciones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Funcion'], 'required'],
            [['Funcion'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Funcion' => 'Funcion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioProyectoFuncion()
    {
        return $this->hasOne(UsuarioProyectoFuncion::className(), ['idFuncion' => 'id']);
    }
}
