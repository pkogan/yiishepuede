<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "requerimientos".
 *
 * @property integer $idRequerimientos
 * @property string $nombreRequerimiento
 * @property string $actores
 * @property string $descripcion
 * @property double $costoReal
 * @property string $datosUtilizados
 * @property integer $idComplejidadRequerimiento
 * @property integer $idEstadoRequerimiento
 * @property integer $idUsuarioRequerimiento
 * @property integer $idProyectoRequerimiento
 *
 * @property Proyecto $idProyectoRequerimiento0
 * @property Estadorequerimiento $idEstadoRequerimiento0
 * @property Usuario $idUsuarioRequerimiento0
 * @property Complejidad $idComplejidadRequerimiento0
 * @property Usuariosasignados $usuariosasignados
 * @property Usuario[] $idUsuarios
 */
class Metricas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'requerimientos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombreRequerimiento', 'costoReal'], 'required'],
            [['costoReal', 'costoRealFinal'], 'number'],
            [['idComplejidadRequerimiento', 'idEstadoRequerimiento', 'idUsuarioRequerimiento', 'idProyectoRequerimiento'], 'integer'],
            [['nombreRequerimiento', 'actores', 'datosUtilizados'], 'string', 'max' => 100],
            [['descripcion'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idRequerimientos' => 'Id Requerimientos',
            'nombreRequerimiento' => 'Nombre Requerimiento',
            'actores' => 'Actores',
            'descripcion' => 'Descripcion',
            'costoReal' => 'Costo Real',
            
            'costoRealFinal' => 'Costo Real (hs)',
            //'costoReal' * $this->getIdProyectoRequerimiento0()->ValorHora => 'valorEstimado',
            'datosUtilizados' => 'Datos Utilizados',
            'idComplejidadRequerimiento' => 'Id Complejidad Requerimiento',
            'idEstadoRequerimiento' => 'Id Estado Requerimiento',
            'idUsuarioRequerimiento' => 'Id Usuario Requerimiento',
            'idProyectoRequerimiento' => 'Id Proyecto Requerimiento',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProyectoRequerimiento0()
    {
        return $this->hasOne(Proyecto::className(), ['id' => 'idProyectoRequerimiento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstadoRequerimiento0()
    {
        return $this->hasOne(Estadorequerimiento::className(), ['idEstado' => 'idEstadoRequerimiento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuarioRequerimiento0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idUsuarioRequerimiento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdComplejidadRequerimiento0()
    {
        return $this->hasOne(Complejidad::className(), ['idComplejidad' => 'idComplejidadRequerimiento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuariosasignados()
    {
        return $this->hasOne(Usuariosasignados::className(), ['idRequerimiento' => 'idRequerimientos']);
    }
     public function getIdRequerimientos()
    {
        return 'idRequerimientos';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['id' => 'idUsuario'])->viaTable('usuariosasignados', ['idRequerimiento' => 'idRequerimientos']);
    }
    
    public function getCostoEstimadoHoras()
    {
        return $this->idComplejidadRequerimiento0->puntos*$this->idProyectoRequerimiento0->TiempoAjustadoPuntos;
    }
    public function getCostoEstimadoPesos()
    {
        return $this->getCostoEstimadoHoras()*$this->idProyectoRequerimiento0->ValorHora;
    }
    public function getCostoRealPesos()
    {
        return $this->costoReal*$this->idProyectoRequerimiento0->ValorHora;
    }
    /*
     public function getTotals($ids)
        {
                $connection = new \yii\db\Connection();
                $connection->open();
                $ids = implode(",",$ids);
                
                //$connection=Yii::app()->db;
                //$command=Model::createCommand("SELECT ifnull(sum(convert(ifnull(costoReal,0), decimal)),0) as total FROM requerimientos where idProyectoRequerimiento in ($ids)");
                $command = $connection->createCommand('SELECT ifnull(sum(convert(ifnull(costoReal,0), decimal)),0) as total FROM requerimientos where idProyectoRequerimiento in ($ids) ');
                $amount = $command->queryAll();
                return "Total Rs: ".$amount; 
        }
     * 
     */
    
    
    public function costoEstimadoProyHoras(){
        $coeph=0;
        foreach ($this->requerimientoses as $proyectoRequerimiento) {
            
            $coeph = $coeph+$proyectoRequerimiento->costoEstimadoReqHoras();
           
            }
         return $coeph;    
     }

/**
    * Costo estimado del proyecto en $ = CEPH * valor hora  costo Estimado del Proyecto en Horas
    * 
    * @return integer Devuelve el costo estimado del Proyecto en horas
    */

     public function costoEstimadoProyPesos() {

        $coePPesos=0;
        $coePPesos=$coePPesos+ ($this->costoEstimadoProyHoras()*$this->ValorHora);
        
        return $coePPesos;
 
    }
/**
    * Duración estimada del proyecto semanas = CEPH / Horas semana
    * Devuelve Falso si HorasSemanales es cero.
    * @return falso si horasSemanales es cero sino la duraciond del Proyecto expresadas en Semana
 */
 
  
    
     public function duracionProySemanas() {

        $duracionSemanas=false;
        if ($this->HorasSemanales <> 0 ){
            $duracionSemanas=$this->costoEstimadoProyHoras()/$this->HorasSemanales;
        }  
        
        return $duracionSemanas;
 
    }  
    
        public function costoRealProyHoras(){
        $corph=0;
        foreach ($this->requerimientoses as $proyectoRequerimiento) {
            
            $corph = $corph+$proyectoRequerimiento->costoReal;
           
            }
         return $corph;    
     }
}
