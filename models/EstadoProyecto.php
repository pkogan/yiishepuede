<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "EstadoProyecto".
 *
 * @property integer $id
 * @property string $EstadoProyecto
 *
 * @property Proyecto[] $proyectos
 */
class EstadoProyecto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'EstadoProyecto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['EstadoProyecto'], 'required'],
            [['EstadoProyecto'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'EstadoProyecto' => 'Estado Proyecto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyectos()
    {
        return $this->hasMany(Proyecto::className(), ['idEstado' => 'id']);
    }
}
