<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Proyecto".
 *
 * @property integer $id
 * @property string $Proyecto
 * @property string $Descripcion
 * @property string $FechaIncicio
 * @property string $FechaFin
 * @property integer $idEstado
 * @property double $ValorHora
 * @property integer $HorasSemanales
 * @property double $TiempoAjustadoPuntos
 *
 * @property EstadoProyecto $idEstado0
 * @property UsuarioProyectoFuncion[] $usuarioProyectoFuncion
 * @property Articulo[] $articulos
 * @property EdmDocumento[] $edmDocumentos
 * @property Etapaproyecto[] $etapaproyectos
 * @property Incidentes[] $incidentes
 * @property Requerimientos[] $requerimientos
 * @property Test[] $tests
 */
class Proyecto extends \yii\db\ActiveRecord
{
    
    /**
     * constante que refiere al id de la actividad Lider de Proyecto
     */
    const LIDERPROYECTO = 1;
    /**
     * constante que refiere al id de la actividad Cliente
     */
    const CLIENTE = 5;
        /**
     * Devuelve verdadero si el usuario de $idUsuario cumple la función idFuncion
     * Si idFunción es null devuelve verdadero si el usuario está vinculado al proyecto
     * @param int $idUsuario id del Usuario
     * @param int $idFuncion id de la Función a validar <5
     * @return boolean
     */
    public function usuarioVinculado($idUsuario,$idFuncion=null) {
        if(!is_null($idFuncion)&&  is_int($idFuncion)){
            if(0>$idFuncion || $idFuncion>5){
                throw new Exception('Error idFunción inválido');
            }
        }
        $usuarioProyecto = false;
        foreach ($this->usuarioProyectoFuncion as $UsuarioFuncion) {
            if ($idUsuario == $UsuarioFuncion->idUsuario && (is_null($idFuncion) || $UsuarioFuncion->idFuncion == $idFuncion)) {
                $usuarioProyecto = true;
                break;
            }
        }
        return $usuarioProyecto;
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Proyecto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Proyecto', 'Descripcion', 'FechaIncicio', 'idEstado', 'ValorHora', 'HorasSemanales', 'TiempoAjustadoPuntos'], 'required'],
            [['Descripcion'], 'string'],
            [['FechaIncicio', 'FechaFin'], 'safe'],
            [['idEstado', 'HorasSemanales'], 'integer'],
            [['ValorHora', 'TiempoAjustadoPuntos'], 'number'],
            [['Proyecto'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Proyecto' => 'Proyecto',
            'Descripcion' => 'Descripcion',
            'FechaIncicio' => 'Fecha Incicio',
            'FechaFin' => 'Fecha Fin',
            'idEstado' => 'Id Estado',
            'ValorHora' => 'Valor Hora',
            'HorasSemanales' => 'Horas Semanales',
            'TiempoAjustadoPuntos' => 'Tiempo Ajustado Puntos',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstado0()
    {
        return $this->hasOne(EstadoProyecto::className(), ['id' => 'idEstado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioProyectoFuncion()
    {
        return $this->hasMany(UsuarioProyectoFuncion::className(), ['idProyecto' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticulos()
    {
        return $this->hasMany(Articulo::className(), ['idProyecto' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEdmDocumentos()
    {
        return $this->hasMany(EdmDocumento::className(), ['documento_proyecto' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEtapaproyectos()
    {
        return $this->hasMany(Etapaproyecto::className(), ['idProyecto' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncidentes()
    {
        return $this->hasMany(Incidentes::className(), ['idProyecto' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequerimientos()
    {
        return $this->hasMany(Requerimientos::className(), ['idProyectoRequerimiento' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTests()
    {
        return $this->hasMany(Test::className(), ['id_proyecto' => 'id']);
    }
}
