<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "severidadincidente".
 *
 * @property integer $id
 * @property string $severidad
 *
 * @property Incidentes[] $incidentes
 */
class Severidadincidente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'severidadincidente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['severidad'], 'required'],
            [['severidad'], 'string', 'max' => 25]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'severidad' => 'Severidad',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncidentes()
    {
        return $this->hasMany(Incidentes::className(), ['idSeveridad' => 'id']);
    }
}
