<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Usuario".
 *
 * @property string $Usuario
 * @property integer $id
 * @property string $Clave
 * @property integer $idRol
 * @property string $NombreApellido
 * @property string $Email
 *
 * @property Rol $idRol0
 * @property UsuarioProyectoFuncion $usuarioProyectoFuncion
 * @property Articulo[] $articulos
 * @property Cambioestados[] $cambioestados
 * @property EdmCambios[] $edmCambios
 * @property EdmDocumento[] $edmDocumentos
 * @property Incidentes[] $incidentes
 * @property Log[] $logs
 * @property Logincidentes[] $logincidentes
 * @property Recursoproyecto $recursoproyecto
 * @property Etapaproyecto[] $idEtapas
 * @property Requerimientos[] $requerimientos
 * @property Test[] $tests
 * @property Usuariosasignados $usuariosasignados
 * @property Requerimientos[] $idRequerimientos
 */
class Usuario extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
   /*********** agregar para interface *********************/
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }
    
    public static function findIdentityByAccessToken($token)
    {
        return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->Clave;
    }

    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }
/*agregar para funcionamiento*/    
    public static function findByUsername($username)
    {
        return static::findOne(['Usuario' => $username]);
    }
    public function validatePassword($authKey)
    {
        return $this->Clave === $authKey;
    }
    public function getUsername()
    {
        return $this->Usuario;
    }
    public function getRol(){
        return $this->idRol0->Rol;
    }
/********************************/    
    public static function tableName()
    {
        return 'Usuario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Usuario', 'Clave', 'idRol', 'NombreApellido', 'Email'], 'required'],
            [['idRol'], 'integer'],
            [['Usuario', 'NombreApellido', 'Email'], 'string', 'max' => 255],
            [['Clave'], 'string', 'max' => 32],
            [['Usuario'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Usuario' => 'Usuario',
            'id' => 'ID',
            'Clave' => 'Clave',
            'idRol' => 'Id Rol',
            'NombreApellido' => 'Nombre Apellido',
            'Email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRol0()
    {
        return $this->hasOne(Rol::className(), ['id' => 'idRol']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioProyectoFuncion()
    {
        return $this->hasOne(UsuarioProyectoFuncion::className(), ['idUsuario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticulos()
    {
        return $this->hasMany(Articulo::className(), ['idUsuario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCambioestados()
    {
        return $this->hasMany(Cambioestados::className(), ['id_usuario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEdmCambios()
    {
        return $this->hasMany(EdmCambios::className(), ['cambio_usuario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEdmDocumentos()
    {
        return $this->hasMany(EdmDocumento::className(), ['documento_usuario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncidentes()
    {
        return $this->hasMany(Incidentes::className(), ['idUsuario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogs()
    {
        return $this->hasMany(Log::className(), ['idUsuario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogincidentes()
    {
        return $this->hasMany(Logincidentes::className(), ['idUsuario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecursoproyecto()
    {
        return $this->hasOne(Recursoproyecto::className(), ['idUsuario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEtapas()
    {
        return $this->hasMany(Etapaproyecto::className(), ['idEtapa' => 'idEtapa'])->viaTable('recursoproyecto', ['idUsuario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequerimientos()
    {
        return $this->hasMany(Requerimientos::className(), ['idUsuarioRequerimiento' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTests()
    {
        return $this->hasMany(Test::className(), ['id_usuario_registro' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuariosasignados()
    {
        return $this->hasOne(Usuariosasignados::className(), ['idUsuario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRequerimientos()
    {
        return $this->hasMany(Requerimientos::className(), ['idRequerimientos' => 'idRequerimiento'])->viaTable('usuariosasignados', ['idUsuario' => 'id']);
    }
}
