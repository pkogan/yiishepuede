<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipoincidente".
 *
 * @property integer $id
 * @property string $tipo
 *
 * @property Incidentes[] $incidentes
 */
class Tipoincidente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipoincidente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo'], 'required'],
            [['tipo'], 'string', 'max' => 25]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo' => 'Tipo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncidentes()
    {
        return $this->hasMany(Incidentes::className(), ['idTipo' => 'id']);
    }
}
