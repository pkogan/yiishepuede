<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "requerimientos".
 *
 * @property integer $idRequerimientos
 * @property string $nombreRequerimiento
 * @property string $actores
 * @property string $descripcion
 * @property double $costoReal
 * @property string $datosUtilizados
 * @property integer $idComplejidadRequerimiento
 * @property integer $idEstadoRequerimiento
 * @property integer $idUsuarioRequerimiento
 * @property integer $idProyectoRequerimiento
 *
 * @property Proyecto $idProyectoRequerimiento0
 * @property Estadorequerimiento $idEstadoRequerimiento0
 * @property Usuario $idUsuarioRequerimiento0
 * @property Complejidad $idComplejidadRequerimiento0
 * @property Usuariosasignados $usuariosasignados
 * @property Usuario[] $idUsuarios
 */
class Requerimientos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'requerimientos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombreRequerimiento', 'costoReal'], 'required'],
            [['costoReal'], 'number'],
            [['idComplejidadRequerimiento', 'idEstadoRequerimiento', 'idUsuarioRequerimiento', 'idProyectoRequerimiento'], 'integer'],
            [['nombreRequerimiento', 'actores', 'datosUtilizados'], 'string', 'max' => 100],
            [['descripcion'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idRequerimientos' => 'Id Requerimientos',
            'nombreRequerimiento' => 'Nombre Requerimiento',
            'actores' => 'Actores',
            'descripcion' => 'Descripcion',
            'costoReal' => 'Costo Real',
            'datosUtilizados' => 'Datos Utilizados',
            'idComplejidadRequerimiento' => 'Id Complejidad Requerimiento',
            'idEstadoRequerimiento' => 'Id Estado Requerimiento',
            'idUsuarioRequerimiento' => 'Id Usuario Requerimiento',
            'idProyectoRequerimiento' => 'Id Proyecto Requerimiento',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProyectoRequerimiento0()
    {
        return $this->hasOne(Proyecto::className(), ['id' => 'idProyectoRequerimiento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstadoRequerimiento0()
    {
        return $this->hasOne(Estadorequerimiento::className(), ['idEstado' => 'idEstadoRequerimiento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuarioRequerimiento0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idUsuarioRequerimiento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdComplejidadRequerimiento0()
    {
        return $this->hasOne(Complejidad::className(), ['idComplejidad' => 'idComplejidadRequerimiento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuariosasignados()
    {
        return $this->hasOne(Usuariosasignados::className(), ['idRequerimiento' => 'idRequerimientos']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['id' => 'idUsuario'])->viaTable('usuariosasignados', ['idRequerimiento' => 'idRequerimientos']);
    }
    
    public function getCostoEstimadoHoras()
    {
        return $this->idComplejidadRequerimiento0->puntos*$this->idProyectoRequerimiento0->TiempoAjustadoPuntos;
    }
}
