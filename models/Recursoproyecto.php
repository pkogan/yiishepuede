<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recursoproyecto".
 *
 * @property integer $idEtapa
 * @property integer $idUsuario
 *
 * @property Etapaproyecto $idEtapa0
 * @property Usuario $idUsuario0
 */
class RecursoProyecto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recursoproyecto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idEtapa', 'idUsuario'], 'required'],
            [['idEtapa', 'idUsuario'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idEtapa' => 'Id Etapa',
            'idUsuario' => 'Id Usuario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEtapa0()
    {
        return $this->hasOne(Etapaproyecto::className(), ['idEtapa' => 'idEtapa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuario0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idUsuario']);
    }
}
