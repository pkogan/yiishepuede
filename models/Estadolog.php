<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estadolog".
 *
 * @property integer $id
 * @property string $descripcion
 *
 * @property Logincidentes[] $logincidentes
 */
class Estadolog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estadolog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descripcion'], 'required'],
            [['descripcion'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogincidentes()
    {
        return $this->hasMany(Logincidentes::className(), ['idEstado' => 'id']);
    }
}
