<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "articulo".
 *
 * @property integer $idArticulo
 * @property integer $idProyecto
 * @property string $titulo
 * @property string $texto
 * @property string $fechaAlta
 * @property string $fecha
 * @property integer $idUsuario
 *
 * @property Proyecto $idProyecto0
 * @property Usuario $idUsuario0
 * @property Log[] $logs
 */
class Articulo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'articulo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idProyecto', 'titulo', 'texto', 'fechaAlta', 'fecha', 'idUsuario'], 'required'],
            [['idProyecto', 'idUsuario'], 'integer'],
            [['texto'], 'string'],
            [['fechaAlta', 'fecha'], 'safe'],
            [['titulo'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idArticulo' => 'Id Articulo',
            'idProyecto' => 'Id Proyecto',
            'titulo' => 'Titulo',
            'texto' => 'Texto',
            'fechaAlta' => 'Fecha Alta',
            'fecha' => 'Fecha',
            'idUsuario' => 'Id Usuario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProyecto0()
    {
        return $this->hasOne(Proyecto::className(), ['id' => 'idProyecto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuario0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idUsuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogs()
    {
        return $this->hasMany(Log::className(), ['idArticulo' => 'idArticulo']);
    }
}
