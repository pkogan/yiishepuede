<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "test".
 *
 * @property integer $id
 * @property integer $id_proyecto
 * @property string $descripcion
 * @property string $resultadoesperado
 * @property string $pasos
 * @property string $prioridad
 * @property integer $id_tipo
 * @property string $fechahora
 * @property integer $id_usuario_registro
 *
 * @property Cambioestados[] $cambioestados
 * @property Proyecto $idProyecto
 * @property Tipotest $idTipo
 * @property Usuario $idUsuarioRegistro
 */
class Test extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_proyecto', 'prioridad', 'id_tipo', 'id_usuario_registro'], 'required'],
            [['id_proyecto', 'id_tipo', 'id_usuario_registro'], 'integer'],
            [['fechahora'], 'safe'],
            [['descripcion', 'resultadoesperado', 'pasos'], 'string', 'max' => 250],
            [['prioridad'], 'string', 'max' => 25]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_proyecto' => 'Id Proyecto',
            'descripcion' => 'Descripcion',
            'resultadoesperado' => 'Resultadoesperado',
            'pasos' => 'Pasos',
            'prioridad' => 'Prioridad',
            'id_tipo' => 'Id Tipo',
            'fechahora' => 'Fechahora',
            'id_usuario_registro' => 'Id Usuario Registro',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCambioestados()
    {
        return $this->hasMany(Cambioestados::className(), ['id_test' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProyecto()
    {
        return $this->hasOne(Proyecto::className(), ['id' => 'id_proyecto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTipo()
    {
        return $this->hasOne(Tipotest::className(), ['id' => 'id_tipo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuarioRegistro()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'id_usuario_registro']);
    }
}
