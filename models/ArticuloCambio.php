<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log".
 *
 * @property integer $idLog
 * @property integer $idArticulo
 * @property string $titulo
 * @property string $texto
 * @property string $fecha
 * @property integer $idUsuario
 *
 * @property Articulo $idArticulo0
 * @property Usuario $idUsuario0
 */
class ArticuloCambio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idArticulo', 'titulo', 'texto', 'fecha', 'idUsuario'], 'required'],
            [['idArticulo', 'idUsuario'], 'integer'],
            [['texto'], 'string'],
            [['fecha'], 'safe'],
            [['titulo'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idLog' => 'Id Log',
            'idArticulo' => 'Id Articulo',
            'titulo' => 'Titulo',
            'texto' => 'Texto',
            'fecha' => 'Fecha',
            'idUsuario' => 'Id Usuario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdArticulo0()
    {
        return $this->hasOne(Articulo::className(), ['idArticulo' => 'idArticulo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuario0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idUsuario']);
    }
}
