<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "etapaproyecto".
 *
 * @property integer $idEtapa
 * @property integer $idProyecto
 * @property string $nombre
 * @property string $descripcion
 * @property string $fechaInicio
 * @property string $fechaFin
 * @property double $porcentaje
 *
 * @property Proyecto $idProyecto0
 * @property Recursoproyecto $recursoproyecto
 * @property Usuario[] $idUsuarios
 */
class Etapaproyecto extends \yii\db\ActiveRecord
{
    
     /**
     * constante que refiere al id de la actividad Lider de Proyecto
     */
    const LIDERPROYECTO = 1;
    /**
     * constante que refiere al id de la actividad Cliente
     */
    const CLIENTE = 5;
        /**
     * Devuelve verdadero si el usuario de $idUsuario cumple la función idFuncion
     * Si idFunción es null devuelve verdadero si el usuario está vinculado al proyecto
     * @param int $idUsuario id del Usuario
     * @param int $idFuncion id de la Función a validar <5
     * @return boolean
     */
    /**public function usuarioVinculado($idUsuario,$idFuncion=null) {
        if(!is_null($idFuncion)&&  is_int($idFuncion)){
            if(0>$idFuncion || $idFuncion>5){
                throw new Exception('Error idFunción inválido');
            }
        }
        $usuarioProyecto = false;
        foreach ($this->usuarioProyectoFuncion as $UsuarioFuncion) {
            if ($idUsuario == $UsuarioFuncion->idUsuario && (is_null($idFuncion) || $UsuarioFuncion->idFuncion == $idFuncion)) {
                $usuarioProyecto = true;
                break;
            }
        }
        return $usuarioProyecto;
    }**/
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'etapaproyecto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idProyecto', 'nombre', 'fechaInicio', 'porcentaje'], 'required'],
            [['idProyecto'], 'integer'],
            [['descripcion'], 'string'],
            [['fechaInicio', 'fechaFin'], 'safe'],
            [['porcentaje'], 'number'],
            [['nombre'], 'string', 'max' => 1000],
            ['fechaFin', 'compare', 'compareAttribute' => 'fechaInicio', 'operator' => '>']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idEtapa' => 'Id Etapa',
            'idProyecto' => 'Id Proyecto',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'fechaInicio' => 'Fecha Inicio',
            'fechaFin' => 'Fecha Fin',
            'porcentaje' => 'Porcentaje',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProyecto0()
    {
        return $this->hasOne(Proyecto::className(), ['id' => 'idProyecto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecursoproyecto()
    {
        return $this->hasOne(Recursoproyecto::className(), ['idEtapa' => 'idEtapa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['id' => 'idUsuario'])->viaTable('recursoproyecto', ['idEtapa' => 'idEtapa']);
    }
    
}
