<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Proyecto;

/**
 * ProyectoSearch represents the model behind the search form about `app\models\Proyecto`.
 */
class ProyectoSearch extends Proyecto
{
    public function rules()
    {
        return [
            [['id', 'idEstado', 'HorasSemanales'], 'integer'],
            [['Proyecto', 'Descripcion', 'FechaIncicio', 'FechaFin'], 'safe'],
            [['ValorHora', 'TiempoAjustadoPuntos'], 'number'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Proyecto::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'FechaIncicio' => $this->FechaIncicio,
            'FechaFin' => $this->FechaFin,
            'idEstado' => $this->idEstado,
            'ValorHora' => $this->ValorHora,
            'HorasSemanales' => $this->HorasSemanales,
            'TiempoAjustadoPuntos' => $this->TiempoAjustadoPuntos,
        ]);

        $query->andFilterWhere(['like', 'Proyecto', $this->Proyecto])
            ->andFilterWhere(['like', 'Descripcion', $this->Descripcion]);

        return $dataProvider;
    }
}
