<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cambioestados".
 *
 * @property integer $id
 * @property integer $id_estado
 * @property integer $id_test
 * @property integer $id_usuario
 * @property string $descripcion
 * @property string $fechahora
 *
 * @property Test $idTest
 * @property Estados $idEstado
 * @property Usuario $idUsuario
 */
class Cambioestados extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cambioestados';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_estado', 'id_test', 'id_usuario', 'descripcion'], 'required'],
            [['id_estado', 'id_test', 'id_usuario'], 'integer'],
            [['fechahora'], 'safe'],
            [['descripcion'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_estado' => 'Id Estado',
            'id_test' => 'Id Test',
            'id_usuario' => 'Id Usuario',
            'descripcion' => 'Descripcion',
            'fechahora' => 'Fechahora',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTest()
    {
        return $this->hasOne(Test::className(), ['id' => 'id_test']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstado()
    {
        return $this->hasOne(Estados::className(), ['id' => 'id_estado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'id_usuario']);
    }
}
