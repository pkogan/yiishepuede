<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logincidentes".
 *
 * @property integer $idLog
 * @property integer $idIncidente
 * @property string $fecha
 * @property integer $idUsuario
 * @property string $descripcion
 * @property integer $idEstado
 *
 * @property Incidentes $idIncidente0
 * @property Usuario $idUsuario0
 * @property Estadolog $idEstado0
 */
class Logincidentes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logincidentes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idIncidente', 'fecha', 'idUsuario', 'descripcion', 'idEstado'], 'required'],
            [['idIncidente', 'idUsuario', 'idEstado'], 'integer'],
            [['fecha'], 'safe'],
            [['descripcion'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idLog' => 'Id Log',
            'idIncidente' => 'Id Incidente',
            'fecha' => 'Fecha',
            'idUsuario' => 'Id Usuario',
            'descripcion' => 'Descripcion',
            'idEstado' => 'Id Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIncidente0()
    {
        return $this->hasOne(Incidentes::className(), ['idIncidente' => 'idIncidente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuario0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idUsuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstado0()
    {
        return $this->hasOne(Estadolog::className(), ['id' => 'idEstado']);
    }
}
