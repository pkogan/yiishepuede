<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuariosasignados".
 *
 * @property integer $idUsuario
 * @property integer $idRequerimiento
 *
 * @property Requerimientos $idRequerimiento0
 * @property Usuario $idUsuario0
 */
class Usuariosasignados extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usuariosasignados';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idUsuario', 'idRequerimiento'], 'required'],
            [['idUsuario', 'idRequerimiento'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idUsuario' => 'Id Usuario',
            'idRequerimiento' => 'Id Requerimiento',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRequerimiento0()
    {
        return $this->hasOne(Requerimientos::className(), ['idRequerimientos' => 'idRequerimiento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuario0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idUsuario']);
    }
}
