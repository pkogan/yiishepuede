<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estadorequerimiento".
 *
 * @property integer $idEstado
 * @property string $DescripcionEstado
 *
 * @property Requerimientos[] $requerimientos
 */
class Estadorequerimiento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estadorequerimiento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['DescripcionEstado'], 'required'],
            [['DescripcionEstado'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idEstado' => 'Id Estado',
            'DescripcionEstado' => 'Descripcion Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequerimientos()
    {
        return $this->hasMany(Requerimientos::className(), ['idEstadoRequerimiento' => 'idEstado']);
    }
}
