<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "incidentes".
 *
 * @property integer $idIncidente
 * @property string $nombre
 * @property integer $idUsuario
 * @property integer $idProyecto
 * @property string $fechaAlta
 * @property string $descripcion
 * @property integer $idSeveridad
 * @property integer $idTipo
 *
 * @property Usuario $idUsuario0
 * @property Proyecto $idProyecto0
 * @property Severidadincidente $idSeveridad0
 * @property Tipoincidente $idTipo0
 * @property Logincidentes[] $logincidentes
 */
class Incidentes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'incidentes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'idUsuario', 'idProyecto', 'fechaAlta', 'descripcion', 'idSeveridad', 'idTipo'], 'required'],
            [['idUsuario', 'idProyecto', 'idSeveridad', 'idTipo'], 'integer'],
            [['fechaAlta'], 'safe'],
            [['descripcion'], 'string'],
            [['nombre'], 'string', 'max' => 25]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idIncidente' => 'Id Incidente',
            'nombre' => 'Nombre',
            'idUsuario' => 'Id Usuario',
            'idProyecto' => 'Id Proyecto',
            'fechaAlta' => 'Fecha Alta',
            'descripcion' => 'Descripcion',
            'idSeveridad' => 'Id Severidad',
            'idTipo' => 'Id Tipo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuario0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idUsuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProyecto0()
    {
        return $this->hasOne(Proyecto::className(), ['id' => 'idProyecto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSeveridad0()
    {
        return $this->hasOne(Severidadincidente::className(), ['id' => 'idSeveridad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTipo0()
    {
        return $this->hasOne(Tipoincidente::className(), ['id' => 'idTipo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogincidentes()
    {
        return $this->hasMany(Logincidentes::className(), ['idIncidente' => 'idIncidente']);
    }
}
