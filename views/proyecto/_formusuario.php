<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Usuario;
use app\models\Funciones;
use yii\helpers\ArrayHelper;
use app\models\UsuarioProyectoFuncion;
/**
 * @var yii\web\View $this
 * @var app\models\UsuarioProyectoFuncion $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="usuario-proyecto-funcion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idUsuario')->dropDownList(ArrayHelper::map(Usuario::find()->all(),'id','Usuario'));?>
    <?php 
//    ejemplo de combo desde query asocindo dos tablas
//    $usuarios=UsuarioProyectoFuncion::find()->with('idUsuario0')->where('idProyecto=2')->distinct()->all();
//    
//    //print_r($usuarios);
//    $select=array();
//    foreach ($usuarios as $usuario){
//        /*
//         * @var app\models\Usuario $usuario 
//         */
//        $select[$usuario->idUsuario]=$usuario->idUsuario0->NombreApellido;//Usuario;
//    }
//    //print_r($select);
//    echo $form->field($model, 'idUsuario')->dropDownList($select);
    ?>
    

    <?= ''//$form->field($model, 'idProyecto')->textInput() ?>

    <?= $form->field($model, 'idFuncion')->dropDownList(ArrayHelper::map(Funciones::find()->all(),'id','Funcion')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
