<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use app\widgets\NuestroNav;

/**
 * @var yii\web\View $this
 * @var app\models\Proyecto $model
 */

$this->title = $model->Proyecto;
$this->params['breadcrumbs'][] = ['label' => 'Proyectos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo NuestroNav::widget(['idProyecto'=>$model->id]);

?>
<div class="proyecto-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            //'Proyecto',
            'Descripcion:ntext',
            'FechaIncicio',
            'FechaFin',
            'idEstado0.EstadoProyecto',
            /*'ValorHora',
            'HorasSemanales',
            'TiempoAjustadoPuntos',*/
        ],
    ]) ?>
    <?php Modal::begin([
    'header' => '<h2>Asignar Usuario al proyecto</h2>',
    'toggleButton' => ['label' => 'Nuevo Usuario',
                       'class' => 'btn btn-success',
        ],
    ]);
        echo $this->render('_formusuario', [
        'model' => $modelUsuario,
    ]);

    Modal::end();
 ?>
    
        <h2>Usuarios Asignados al proyecto</h2>
        
     <?= GridView::widget([
        'dataProvider' => $dataUsuarioProvider,
        'filterModel' => $searchUsuarioModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idUsuario0.NombreApellido',
            'idFuncion0.Funcion',

            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{delete}',
                'controller'=>'usuario-proyecto-funcion',
                ]
        ],
    ]); ?>
    

</div>
