<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\Proyecto $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="proyecto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Proyecto')->textInput(['maxlength' => 1000]) ?>

    <?= $form->field($model, 'Descripcion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'FechaIncicio')->textInput() ?>

    <?= $form->field($model, 'idEstado')->textInput() ?>

    <?= $form->field($model, 'ValorHora')->textInput() ?>

    <?= $form->field($model, 'HorasSemanales')->textInput() ?>

    <?= $form->field($model, 'TiempoAjustadoPuntos')->textInput() ?>

    <?= $form->field($model, 'FechaFin')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
