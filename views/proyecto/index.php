<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\ProyectoSearch $searchModel
 */

$this->title = 'Proyectos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proyecto-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Proyecto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'Proyecto',
            'Descripcion:ntext',
            'FechaIncicio',
            'FechaFin',
            // 'idEstado',
            // 'ValorHora',
            // 'HorasSemanales',
            // 'TiempoAjustadoPuntos',

            ['class' => 'yii\grid\ActionColumn',
                
                'template' => '{view}{update}{delete}{tareas}',
                
                    
         
            ],
        ]
    ]);
    ?>

</div>
