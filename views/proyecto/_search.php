<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\ProyectoSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="proyecto-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'Proyecto') ?>

    <?= $form->field($model, 'Descripcion') ?>

    <?= $form->field($model, 'FechaIncicio') ?>

    <?= $form->field($model, 'FechaFin') ?>

    <?php // echo $form->field($model, 'idEstado') ?>

    <?php // echo $form->field($model, 'ValorHora') ?>

    <?php // echo $form->field($model, 'HorasSemanales') ?>

    <?php // echo $form->field($model, 'TiempoAjustadoPuntos') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
