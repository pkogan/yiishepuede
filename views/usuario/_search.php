<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\UsuarioSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="usuario-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Usuario') ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'Clave') ?>

    <?= $form->field($model, 'idRol') ?>

    <?= $form->field($model, 'NombreApellido') ?>

    <?php // echo $form->field($model, 'Email') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
