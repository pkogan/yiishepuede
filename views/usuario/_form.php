<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\Usuario $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="usuario-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Usuario')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'Clave')->textInput(['maxlength' => 32]) ?>

    <?= $form->field($model, 'idRol')->textInput() ?>

    <?= $form->field($model, 'NombreApellido')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'Email')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
