<?php
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 */
$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
       Sistema de Gestión para un Equipo de Desarrollo. Desarrollado por los alumnos de la materia Programación Web Avanzada de la Tecnicatura en Universitario en Desarrollo Web de la Universidad Nacional del Comahue. 1er Cuatrimestre 2014  
    </p>

</div>
