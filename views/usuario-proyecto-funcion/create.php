<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\UsuarioProyectoFuncion $model
 */

$this->title = 'Create Usuario Proyecto Funcion';
$this->params['breadcrumbs'][] = ['label' => 'Usuario Proyecto Funcions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-proyecto-funcion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
