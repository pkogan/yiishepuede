<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\UsuarioProyectoFuncion $model
 */

$this->title = 'Update Usuario Proyecto Funcion: ' . $model->idUsuario;
$this->params['breadcrumbs'][] = ['label' => 'Usuario Proyecto Funcions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idUsuario, 'url' => ['view', 'idUsuario' => $model->idUsuario, 'idProyecto' => $model->idProyecto, 'idFuncion' => $model->idFuncion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="usuario-proyecto-funcion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
