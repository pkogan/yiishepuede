<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\UsuarioProyectoFuncion $model
 */

$this->title = $model->idUsuario;
$this->params['breadcrumbs'][] = ['label' => 'Usuario Proyecto Funcions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-proyecto-funcion-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idUsuario' => $model->idUsuario, 'idProyecto' => $model->idProyecto, 'idFuncion' => $model->idFuncion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idUsuario' => $model->idUsuario, 'idProyecto' => $model->idProyecto, 'idFuncion' => $model->idFuncion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idUsuario',
            'idProyecto',
            'idFuncion',
        ],
    ]) ?>

</div>
