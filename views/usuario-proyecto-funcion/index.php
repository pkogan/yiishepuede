<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\UsuarioProyectoFuncionSearch $searchModel
 */

$this->title = 'Usuario Proyecto Funcions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-proyecto-funcion-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Usuario Proyecto Funcion', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idUsuario',
            'idProyecto',
            'idFuncion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
